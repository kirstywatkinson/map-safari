<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		
		<link rel="shortcut icon" href="#">
                <!--load osm-auth -->
                <script src="./scripts/osm-auth-main/osmauth.js"></script>
                <!-- load js require -->
                <script src="./node_modules/npm-require/src/npm-require.js"></script>
                <!-- Load CSS for Leaflet -->
                <link rel="stylesheet" href="https://unpkg.com/leaflet@1.7.1/dist/leaflet.css" />
                <!-- Load JavaScript for Leaflet -->
                <script src="https://unpkg.com/leaflet@1.7.1/dist/leaflet.js"></script>
            
                <!-- Load CSS for Context Menu-->
                <link rel="stylesheet" href="./scripts/leaflet.contextmenu.css"/>
                <!-- Load Javascript for Turf -->
                <script src="https://cdn.jsdelivr.net/npm/@turf/turf@5/turf.min.js"></script>
                <!-- Load Javascript for Bing tiles-->
                <script src="./scripts/Bing.js"></script>
                <script src="./scripts/Bing.addon.applyMaxNativeZoom.js"></script>
                <!-- Load fonts -->
                <link href="https://fonts.googleapis.com/css2?family=Heebo:wght@400;600&display=swap" rel="stylesheet">
                <!-- Polyfill-->
                <script src="https://cdn.polyfill.io/v2/polyfill.min.js?features=Promise"></script>
        
		<style>
            body, html{
                        width: 100%;
                        height: 100%;
                        margin: 0;
                }

            /* The map */
            #map {

			position: relative;
			left: 12.5%;
                        width: 70%;
                        height: 75%;
                        z-index: 0;
			text-align: center;
                }
        
            
	ul {
              list-style-type: none;
              margin: 0;
              padding: 0;
              overflow: hidden;
            
                   
                    
                }
            .ul2{
                 height:200px;
                background-image: linear-gradient(rgba(247, 249, 251, 0.7), rgba(247, 249, 251, 0.7)), url(./images/globecolour.png);
                background-position: center;
                background-size: 200px 175px;
                position: relative;
                border-bottom: 1px solid gray;
                
            }

		.ul3{
                display:inline-block;
            }


	   .main-head{
		text-align: center; 
		position: absolute;
		top: 50%;
		left: 50%;
		transform: translate(-50%, -50%);
		color: black;
		font-family: 'Heebo', sans-serif;
		font-size:45px

		}

	p {
                text-align: justify;
                text-justify: inter-word;
                float: center;
                display: inline-block;
                font-family: 'Heebo', sans-serif;
                font-size: 17px;
                line-height: 2;
                max-width: 1500px;
                position: relative;
                left: 50%;
                transform: translate(-50%);
                
            }

		li {
                    float: left;
                    text-align: justify;
                    text-justify: inter-word;
                    display:inline-block;
                }

                li a {
                  display: block;
                  color: black;
                  text-align: center;
                  padding: 8px 16px;
                  text-decoration: none;
                  font-family: 'Noto Sans TC', sans-serif;
                    font-weight: 700;
                }

                li h {
                    display: block;
                    color: black;
                    text-align: center;
                    padding: 8px 16px;
                    text-decoration: none;
                    font-family: 'Noto Sans TC', sans-serif;
                    font-weight: 700;
                    font-size: 16pt;

                }



		/* style the container */
		#button-container {
			text-align: center;
			width: 100%;
		}
	
		/* style the container */
                #login-container {
                        text-align: center;
                        width: 100%;
                }
		
		/* Button styles */
		.button {
			position: relative;
			border: none;
			width: 75px;
			height: 175px;
			display: inline;
			cursor: pointer;
			overflow-wrap: break-word;
			z-index: 500;
			background: none;
		}

		.buttone {

			width: 250px;

		}

		.buttonh {

			width: 250px;
		}

		
		
		.button2{
			margin-top: 15px;
			padding-right: 5px;
                        padding-top: 5px;
                        padding-bottom: 5px;
                        padding-left: 5px;
                        background-color: white;
                        border: black 2px solid;
                        cursor: pointer;
                        font-family: 'Heebo', sans-serif;
                        font-weight: bold;
                        font-size: 16px;
                        width: 100px;
                        color: black;
			z-index: 500;
                }

        
		.button2:hover{
			border: black 3px solid;

		}



		/*Legend specific*/
		.legend {
		  padding: 6px 8px;
		  font: 14px Arial, Helvetica, sans-serif;
		  background: white;
		  background: rgba(255, 255, 255, 0.8);
		  line-height: 24px;
		  color: black;
		}

		.legend span {
		  position: relative;
		  bottom: 3px;
		}

		.legend i {
		  width: 18px;
		  height: 18px;
		  float: left;
		  margin: 0 8px 0 0;
		  opacity: 0.7;
		  border: solid 0.5px black;
		}

		.legend i.icon {

		  background-size: 18px;
		  background-color: rgba(255, 255, 255, 1);
		}

		.footer{
                background-color: #7fcdbb;
                height: 150px;
                width: 100%;
                position: relative;
                color: white;
                font-family: 'Noto Sans TC', sans-serif;
                text-align: center; 
                
            }
            
	    .footer .footer-section{
                position:absolute;
                bottom:5px;
                text-align: center; 
                width:100%;
            }

		img {
			height: 25px;
			width: 25px;

		}

	
		</style>
	</head>
	
	<body>

		 <ul class="ul ul2"> <h class="main-head">ABOUT</h>
            	<li style="float:right"><a href="./home.php">HOME</a></li>
            	</ul>

		
		    <p><b>Insert game name here</b> is part of a larger PhD research project carried out by Kirsty Watkinson at the University of Manchester seeking to create maps of un-mapped parts of the Global South to help address humanitarian and development challenges. It combines a volunteer (you!) with machine learning to speed up the mapping process. By doing this we can map as accurately as a human, but cover much larger areas than if we were to do it alone. This helps us to map remote areas of the world desperately in need of map data to assist humanitarian response, support national development and contribute to the monitoring of global initiatives such as the <b><a href='https://sdgs.un.org/goals' target='_blank'>Sustainable Development Goals</a></b>. Insert game name here also contributes to the work of the <b><a href='https://spark.adobe.com/page/SiM4Iji37rdJr/' target='_blank'>Community Mapping Company</a></b>, also based at the University of Manchester. <br>
            	<br>All data produced will be uploaded to <b><a href='https://www.openstreetmap.org/' target='_blank'>OpenStreetMap</a> </b> where it will be available for free to all.
            	<br>
		<br> Currently we have mapped <b><span id='area'></span> km<sup>2</sup></b> of the Acholi region. See our progress below: 


		<div id="map"></div>
<br>
		<div class="footer">
            <div class="footer-section">
                <ul class="ul ul3">
                <li><p>Contact: kirsty.watkinson@manchester.ac.uk</p></li>
                <li><a href="https://www.instagram.com/community_mapping_ug/" target="_blank"><img src="./images/instaw.png" alt="Instagram"></a></li>
                <li><a href="https://twitter.com/klwatkinson" target="_blank"><img src="./images/twitter.png" alt="Twitter"></a></li>
		<li><a href="https://www.youtube.com/channel/UCngCyaEIbO9ACjXrroBd5ow/featured" target="_blank"><img src="./images/youtube.png" alt="YouTube"></a></li></ul> 
                
            </div>
		</div>

		<?php

			require('./scripts/connection.php');

			$grid = '';
                	$gridRow = '';			

			$gridQuery = pg_query("SELECT id, status, ST_AsGeoJSON(ST_Transform(wkb_geometry, 4326)) FROM grid");

			 while ($row = pg_fetch_row($gridQuery)){

				$gridRow = (strlen($gridRow) > 0 ? ',' : '') . '{"type": "Feature", "id": ' . $row[0] . ', "properties": { "status": ' . $row[1] . '}, "geometry": ' . $row[2] . '}';

                        	$grid .= $gridRow;

                        }

                        $grid = '{"type": "FeatureCollection","name": "grid", "crs":{"type": "name", "properties": { "name": "urn:ogc:def:crs:OGC:1.3:CRS84" } }, "features": [ ' . $grid . ' ]}';




			//Get squares

			$score = "test";

			echo "<script>\n";
			echo "let grid = $grid\n";
			echo"</script>";


			

		?>
<script>
	

		var OsmRequest = require('osm-request');

		const osm = new OsmRequest({
                endpoint: 'https://api06.dev.openstreetmap.org',
                oauthConsumerKey: 'VnCASv0JrF4mqHQ3uWBCE6bf1dHFh14ltHoGBbmV',
                oauthSecret: '9fAkZCyRDkS8yAySv9MJYf78uFM8wcERG3oK1081',
                auto: true});
                
        //Bounds of Gulu district
        min_x = 32.12395;
        max_x = 32.82805;
        min_y = 2.4515;
        max_y = 3.3066;


        //set up map
        //Create a variable containingthe map object and add the basemaps as layers
        map = L.map('map', {minZoom: 9,
                            maxZoom: 10,
                            zoomControl: false
                        });
            
        //Load bing and OpenStreetMap basemaps
        const bing = L.bingLayer('AgWUG-xtDgsFNlDhzIMjiuvn2sqdQ82E_ywrY3jp_mbrOiIgW9w7YJ39oJiKwteg', {
                               maxZoom: 18}).addTo(map);



                                        
	let gridColour = L.geoJSON(grid);
	

	let area = 0;

	gridColour.eachLayer(function(layer){

		if (layer.feature.properties.status === 0){
			layer.setStyle({fillColor: '#edf8b1', weight: 0, fillOpacity: 0.6})
		
		} else if (layer.feature.properties.status === 1){
			layer.setStyle({fillColor: '#7fcdbb', weight: 0, fillOpacity: 0.6})
			
		} else {

			layer.setStyle({fillColor: '#2c7fb8', weight: 0, fillOpacity: 0.6})

			area =+ 1;
		}


		});

	console.log(area);

	document.getElementById('area').innerHTML = area;

	gridColour.addTo(map);

	let gridOutline = L.geoJSON(grid, {style: {color: 'black', weight: 0.75, fillOpacity: 0}}).addTo(map);

	let gridBounds = gridOutline.getBounds();

	let legend = L.control({position: 'bottomright'});

	legend.onAdd = function(map) {
  		let div = L.DomUtil.create("div", "legend");
  			div.innerHTML += '<i style="background: #2c7fb8"></i><span>Mapped</span><br>';
  			div.innerHTML += '<i style="background: #7fcdbb"></i><span>In progress</span><br>';
  			div.innerHTML += '<i style="background: #edf8b1"></i><span>Not mapped</span><br>';
  
  		return div;
	};

	legend.addTo(map);


	map.fitBounds(gridBounds);
                 
        
		</script>
	</body>

</html>
