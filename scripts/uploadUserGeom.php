<?php

/*
* Add user created building to the database
*/

header('Content-Type: application/json');

//Get user created geometry from the main mapping page
$post = file_get_contents('php://input');
$array = json_decode($post, true);

//Get geometry of outline
$geom = $array["geom"];
$geom2 =  json_encode($geom);

//Get tag
$tag = $array["tag"];

//Get username
$user = $array["user"];




//Connect to database 
require('./connection.php');

//Get user id
$id = pg_fetch_row(pg_query("SELECT user_id FROM users WHERE username = '$user'"))[0];

// Add geometry to the database
$query = "INSERT INTO userCreated (username,user_ID,tag, geom) VALUES ('$user','$id','$tag', ST_SetSRID(ST_GeomFromGeoJSON('$geom2'), 4326))";
$result = pg_query($query);

//Update user points
//Get current points
$countQuery = pg_query("SELECT points FROM users WHERE username = '$user'");

//Add 1 to user points
$newCount = pg_fetch_row($countQuery)[0] + 1;
$updateCount = pg_query("UPDATE users SET points = '$newCount' WHERE username = '$user'");

//Update competition points - collective scores

$countQueryh = pg_query("SELECT human FROM competition LIMIT 1");
$newCounth = pg_fetch_row($countQueryh)[0] + 1;
$updateCounth = pg_query("UPDATE competition  SET human = '$newCounth'");
$countQueryC = pg_query("SELECT computer FROM competition LIMIT 1");
$newCountC = pg_fetch_row($countQueryC)[0] - 1;
$updateCountC = pg_query("UPDATE competition SET computer = '$newCountC'");

//Update human vs computer individual scores
$countQueryI = pg_query("SELECT computer FROM users WHERE username = '$user'");
$computer = pg_fetch_row($countQueryI)[0] - 1;
$countQueryI2 = pg_query("SELECT human FROM users WHERE username = '$user'");
$human = pg_fetch_row($countQueryI2)[0] + 1;
$updateCountI = pg_query("UPDATE users SET computer = '$computer', human = '$human' WHERE username = '$user'");

//Test it is working
echo json_encode($query);
?>

