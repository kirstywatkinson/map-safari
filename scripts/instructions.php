<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">      		
		<link rel="shortcut icon" href="#">
                <!--load osm-auth -->
                <script src="./osm-auth-main/osmauth.js"></script>
                <!-- load js require -->
                <script src="../node_modules/npm-require/src/npm-require.js"></script>
                <!-- Load CSS for Leaflet -->
                <link rel="stylesheet" href="https://unpkg.com/leaflet@1.7.1/dist/leaflet.css" />
                <!-- Load JavaScript for Leaflet -->
                <script src="https://unpkg.com/leaflet@1.7.1/dist/leaflet.js"></script>
            
                <!-- Load CSS for Context Menu-->
                <link rel="stylesheet" href="./leaflet.contextmenu.css"/>
                <!-- Load Javascript for Bing tiles-->
                <script src="./Bing.js"></script>
                <script src="./Bing.addon.applyMaxNativeZoom.js"></script>
                <!-- Load fonts -->
                <link href="https://fonts.googleapis.com/css2?family=Heebo:wght@400;600&display=swap" rel="stylesheet">
                <!-- Polyfill-->
                <script src="https://cdn.polyfill.io/v2/polyfill.min.js?features=Promise"></script>
		<script src="https://code.iconify.design/1/1.0.7/iconify.min.js"></script>
        
		<style>
            body, html{
                        width: 100%;
                        height: 100%;
                        margin: 0;
                }

		

		#map {
                        width: 20%;
                        height: 40%;
                        border: 1px solid black;
                        pointer-events:none;
                }




	ul {
              		list-style-type: none;
              		margin: 0;
              		padding: 0;
              		overflow: hidden;
              		background-color: #F7F9FB;
            		background-image: linear-gradient(rgba(247, 249, 251, 0.8), rgba(247, 249, 251, 0.8)), url(../images/logo.png);
                	background-position: center;
            		background-size: 250px 250px;
            		border-bottom: 1px solid gray;
			height:5%;
            	}

		.ul2{
			float: left;
                        clear: left;
                        font-family: 'Noto Sans TC', sans-serif;
                        font-size: 17px;
                        position: relative;
                        padding: 0px 50px 10px 50px;
                        line-height: 1.5;
			margin-left: 5px;

		}


	   .main-head{
		text-align: center; 
		position: absolute;
		top: 50%;
		left: 50%;
		transform: translate(-50%, -50%);
		color: black;
		font-family: 'Heebo', sans-serif;
		font-size:45px
		margin: 0;
		}


		#main-body{

		padding:0.5%;
		width: 100%;
		height:95%;
		}

	/* style the header text */
		#header-text {
			float: left;
			font-family: 'Heebo', sans-serif;
			font-weight: bold;
			font-size: 700;
			padding: 20px 20px 20px 0px;
			clear: both;

	}


	p {
                display: inline-block;
                font-family: 'Heebo', sans-serif;
                font-size: 17px;
                line-height: 2;
                max-width: 1500px;
                position: relative;
            }


		.li2 {
			display: block;
			color: black;
			font-family: 'Heebo', sans-serif;
			font-size: 17px;
			list-style-type: circle;
			max-width: 1500px;
			position: relative;
                left: 50%;
                transform: translate(-50%);
		clear: both;
		}

		li a {
              		display: block;
              		color: black;
              		text-align: center;
              		padding: 18px 16px;
              		text-decoration: none;
              		font-family: 'Noto Sans TC', sans-serif;
                	font-weight: 400;
			float: right;
            }
            
            li h {
                display: block;
                color: black;
                text-align: center;
                padding: 4px 16px;
                text-decoration: none;
                font-family: 'Noto Sans TC', sans-serif;
                font-weight: 700;
                font-size: 24pt;
		float: left;                
            }
            
            li p {
                
                font-family: 'Noto Sans TC', sans-serif;
                font-weight: 700;
                background-color: #ade0d4;
                text-align: center;
                border: 2px solid black;
                padding-left: 3px;
            
            }



		/* style the container */
		#button-container {
			text-align: left;
			width: 100%;
		}
	
		/* style the container */
                #login-container {
                        text-align: center;
                        width: 100%;
                }
		
		/* Button styles */
		.button {
			position: relative;
			border: none;
			width: 6%;
			height: 100%;
			display: inline;
			cursor: pointer;
			overflow-wrap: break-word;
			z-index: 500;
			background: none;
		}

		.buttone {
			width: 16%;
			height: 85%;

		}

		.buttonh {
			width: 13%;
			height: 80%;
		}
		
		.rhino {
                        height:100%;
                        width:100%;

                }

                .giraffe {
                        height:100%;
                        width:100%;
                }

                .elephant {
                        height:100%;
                        width:100%;
                }

                .hippo {
                        height:100%;
                        width:100%;

                }

		
		
		.button2{
                background-color: #87ba4c;
			border: 1px solid #6b943d;
			color: white;
			width: 130px;
			padding: 3px;
			text-align: center;
			display: block;
			font-size: 15px;
			cursor: pointer;
			font-weight: 700;
			font-family: 'Noto Sans TC', sans-serif;
            		float:right;
            		position:relative;
            		top: 14px;
            		right: 5px;    
            	}

		.button2:hover{
			color: black;
		}

		.fake-buttons{
			background-color:#f44336;
			padding:4px;
			border:1px solid black;
			color:black;
			font-weight:700;
			font-size: 17px;
			font-family: 'Heebo',sans-serif;
		}

		.blue {
			 background-color:#008CBA;
			 border:1px solid black;
		}

		.green {
			 background-color:#4CAF50;
			 border:1px solid black;			
		}



		.finished {
			background-color:#fff700; 
			border: 2px solid #000000;

		}

		.complete {
			background-color: #4CAF50;

		}


		.partial {
			background-color:#fff080;

		}




		.white-buttons{

			background-color:white;
			padding:8px;
			border:1px solid black;
			border-radius: 5px; !important;color:black;
			font-size: 13px;
			font-family: Calibri, sans-serif;

		}

		img {
			margin-left: 5%;

		}

		#buildings-example {
			height: 45%;
			width: 85%
		}

		#leaf-img {
			height: 25px;
                        width: 25px;

			margin-left: 0;

		}

		#energy2 {
                         width: 10%;
                        background-color: #ddd;
                        border: 1px black solid;
                }

                #energyBar {
                  width: 75%;
                        max-width: 100%;
                  height: 30px;

                }


		#screenshot{
			height:450px;
			width:625px;

		}


		#rhino2{

			height:105px;
			width:155px;

		}

		#giraffe2 {
			height:180px;
			width:55px;

		}

		#elephant2{

			height:120px;
			width:210px;
		
		}

		#hippo2 {
			height:95px;
			width:170px;
			

		}


		#gif{
			height:243; 
			width:320;

		}


		@viewport {
                        width: device-width ;
                        zoom: 1.0 ;
                }

		@media screen and (max-width: 1366px) and (min-width:500px){

		p {

		font-size: 15px;

		}	


		.li2 {


		font-size: 15px;

		}

		.fake-buttons{

				font-size: 13px;
				break-word: keep-all;
				overflow: visible;
				padding:2px;
		}

		 .white-buttons{

			padding:4px;
		}

		#screenshot{
                        height:400px;
                        width:575px;

                }


		#gif{
                        height:213;
                        width:290;

                }

		#header-text {


		font-size: 15px;

		}


		}

		</style>
	</head>
	
	<body>


		<ul>
              		<li><h></h></li>
                	<li><a href="../About.php" target="_blank">About</a></li>
                	<li><a href="../home.php" target="_blank">Home</a></li>
              		<!--<li style="float:right; padding-left; 10px;"><a class="button2" href="./mapping.php" target="_blank" >ENTER GAME</a></li>-->
              		<!--<li class="zoom" style="margin-right:10px; float: right;"><a class="button2" style="background-color:#3498db; color: white;" target="_blank" href='https://zoom.us/j/8080813849'>Zoom Room</a></li>--> 
	       </ul>

		<div id="main-body">

		<div style="max-width: 1500px; height: 90%;left: 50%; transform: translate(-50%); position: relative">
		<div>
		<p>Firstly, <b>thank you</b> for playing! Not only are you having fun but you are also helping to reach those in need of orthopeadic care and securing safe drinking water for communities in Northern Uganda. 		

		<br>Below are some instructions on how to play, have a read and then have a go!</p>

		</div>

		<div id="header-text"><h>Game objectives</h>
		</div>

		<div>
		<li class="li li2">• Map buildings and score points, both as an individual and team to beat the machine!</li>
		<li class="li li2">• Move up the levels starting your adventure as an interested tourist, as you map you will gain experience and in time become a master of the map!</li>
		<li class="li li2">• Travel through Northern Uganda, mapping buildings and <span><a style="float:none; padding:0px; display:inline-block" href="../about.php" target="_blank">watch</a></span> as we collectively progress across the 28,500 km<sup>2</sup> of the Acholi region!</li> 

		</div>


		<div>
		<p>To get started, pick your avatar. These are all animals we can see in Uganda, especially in their wonderful <a href="https://www.ugandawildlife.org/" target="_blank" style="float:none; padding:0px; display:inline-block">national parks</a>:</p>
		<div id="button-container">

		
		<img src='../images/rhino.png' id="rhino" class="button buttonh"><img class="button" src='../giraffe.png' id="giraffe"></img><img class="button buttone" src='../elephant.png' id="elephant"><img class="button buttonh" src='../hippo.png' id="hippo">

		</div>
		<p>Once you've picked your avatar you'll be taken to the main game page. Here is where you will explore Northern Uganda as your chosen animal and look for buildings that need to be mapped:</p>

		</div>

		<div>
		<img src='../images/game_screenshot.png' id="screenshot">

		</div>

		<div>

		<br><p>To move around use the arrow buttons on the right of your screen or using the arrows on your keyboard:</p>

		<br><p><span class="white-buttons" style="margin-right: 5%"><span class="iconify" data-icon="akar-icons:arrow-up-thick" data-inline="true"></span></span><span class="white-buttons" style="margin-right: 5%"><span class="iconify" data-icon="akar-icons:arrow-left-thick" data-inline="true"></span></span><span class="white-buttons" style="margin-right: 5%"><span class="iconify" data-icon="akar-icons:arrow-right-thick" data-inline="true"></span></span><span class="white-buttons" style="margin-right: 5%"><span class="iconify" data-icon="akar-icons:arrow-down-thick" data-inline="true"></span></span></p> 

		<br><p> Every time you move you will lose energy. You can monitor your energy through the energy bar in the top right corner of your screen: <span class="fake-buttons green" style="color:#4CAF50">energy bar example</span>. To restore some energy you need to look for leaves <img src="../images/leaf.png" id="leaf-img"></img> that are scattered around the map, when you eat them you will gain energy.</p>  


		<p>Now you know how to move, you need to look for things to map. These could be potential buildings detected by a computer using <a href="" target="_blank">machine learning</a> or buildings not found by either the machine or other players.</p>
		 <div style="width: 33%; display:inline-block"><p>Computer predicted buildings will have a blue outline and will look like this:</p> 
		<br><img src='../images/machine_detected.png' id="buildings-example"></img></div>

		 <div style="width: 33%; display:inline-block"><p>Buildings that have already been mapped will have a pink outline and a slightly opaque fill and will look like this:</p>

		<br><img src='../images/mapped_buildings.png' id="buildings-example"></img></div>

		 <div style="width: 33%; display:inline-block"><p>Buildings that haven't been mapped won't have any outlines and will look something like this: </p>

		<br><img src='../images/hut_example.png' id="buildings-example"></img></div>

		<br><p>For more examples of buildings you are likely to come across whilst exploring see the <a href='./building_guidance.html'>building gallery</a>.</p>
		</div>

		<div id="header-text">Mapping features</div>
		<div style="clear:both"><p>Once you find something to map, depending on what it is, there will be two ways to map it.</p>
		</div>
		<div id="header-text"><i>Computer detected features</i></div> 
		<div>
		<p>When you get close to a computer dectected feature the map will zoom in, editing tools will appear on your screen and three buttons will appear on the right of your screen under your move arrows.</p>

		<img src="../images/editing.png" style="height:50%; width:45%"></img>

		<p>You then need to compare the outline to what is underneath on the image. To help you, you can use the <span class="white-buttons">Hide Outline</span> button. This will hide the outline from your view. To see the outline again, press the <span class="white-buttons">Show Outline</span> button.</p>
		<p>If you think the feature is a building and is the right shape and in the correct location, simply click the green <b>Upload building</b> button:</p>
		<br><span class="fake-buttons green">Upload building</span>

		<br><p>If you think the feature isn't a building, click the red <b>Not a building</b> button:</p>

		<br><span class="fake-buttons"> Not a building</span>


		<br><p>If you are not sure, click the blue <b>Not sure</b> button:</p> 

		<br>
            <span class="fake-buttons blue"> Not sure</span>

		<br><p>To edit the outline before uploading you can use the black box with white corner markers to resize, rotate and drag the outline:</p>

		</div>

		<div style="width: 33%; display:inline-block"><div style="text-align:center"><h id="header-text" style="float:none">Rotate</h></div><br><img src="../images/rotate.gif" id="gif"></img></div><div style="width: 33%; display:inline-block"><div style="text-align:center"><h id="header-text" style="float:none">Resize</h></div><br><img src="../images/resize.gif" id="gif"></img></div><div style="width: 33%; display:inline-block"><div style="text-align:center;"><h id="header-text" style="float:none">Drag</h></div><br><img src="../images/drag.gif" id="gif"</img></div>
		<div>
		<br><p>You can also use two buttons at the top of your screen:</p>

		<li style="margin-bottom: 15px" class="li li2"><span class="white-buttons">Make Circle</span> : When pressed, turns your square feature into a circle.</li>
		<li class="li li2"><span class="white-buttons">Reset feature</span> : When pressed, resets your feature to its original form. Handy if you make a mistake.</li>

		
		</div><br>
		<div id="header-text"><i>Creating your own feature</i></div>

		<div><p>When you come across what you think is building that hasn't been detected by the computer or found by a fellow mapper you can map it yourself by clicking the <span class="white-buttons" style="font-weight:700">Add own building</span> button. This will bring up a toolbar in the bottom right of your screen and you can select to map a:</p>
		<li style="margin-bottom: 15px" class="li li2"><span class="white-buttons" style="font-size:20px; padding:5px; height:30px; width:30px"><span class="iconify" data-icon="bi:pentagon-fill" data-inline="true">test</span></span> Polygon</li>
		<li style="margin-bottom: 15px" class="li li2"><span class="white-buttons" style="font-size:20px; padding:5px; height:30px; width:30px"><span class="iconify" data-icon="bi:square-fill" data-inline="true"></span></span> Square</li>
		<li style="margin-bottom: 15px" class="li li2"><span class="white-buttons" style="font-size:20px; padding:5px;height:30px; width:30px"><span class="iconify" data-icon="akar-icons:circle-fill" data-inline="true"></span></span> Circle</li>
</div>
		<p>Which can be drawn as demonstrated below:</p><br>

		<div style="width: 33%; display:inline-block"><div style="text-align:center;"><h id="header-text" style="float:none">Polygon</h></div><br><img src="../images/polygon.gif" id="gif"></div><div style="width: 33%; display:inline-block"><div style="text-align:center"><h id="header-text" style="float:none">Square</h></div><br><img src="../images/square.gif" id="gif"></div><div style="width: 33%; display:inline-block"><div style="text-align:center;"><h id="header-text" style="float:none">Circle</h></div><br><img src="../images/circle.gif" id="gif"></div>

		<p>You will have also noticed two other buttons. The <span class="iconify" data-icon="akar-icons:edit" data-inline="false"></span> button allows you to edit features you have drawn. The <span class="iconify" data-icon="foundation:trash" data-inline="false"></span> button allows you to delete features you have drawn. Demonstrated below:</p><br>

		<div style="width: 33%; display:inline-block"><div style="text-align:center"><h id="header-text" style="float:none">Edit</h></div><br><img src="../images/edit.gif" id="gif"></div><div style="width: 33%; display:inline-block"><div style="text-align:center"><h id="header-text" style="float:none">Delete</h></div><br><img src="../images/delete.gif" id="gif"></div>


		<p>Once you are happy with your drawn buildings, you can upload them by clicking the green <span class="fake-buttons green">Upload building</span> button on the right of your screen. </p>
		<p>To help you navigate around the square a reference square can be found in the top left of your screen. The square is divided into 9 smaller squares. When you start only one square is visible:</p>

		<div id="map"></div>

		<p> As you move into a new square the satellite image in the square will become visible, both on the main and reference map. Once you have explored every square a button will appear in the top right of the screen: <span class="fake-buttons finished"> I'm finished!</span>. When you click on this a pop up will appear asking if you have <span class="fake-buttons complete">Finished mapping!</span> (everything is mapped to the best of your knowledge) or <span class="fake-buttons partial">Given up mapping!</span> (explored all squares but you are not sure if everything is mapped). After this you will be asked if you want to move on to a new square or finish playing. Selecting finish playing will take you to the game statistics page which displays your points and level, as well as the game leader boards.</p>

		<p>And that's everything. If you're ready to play you can start by choosing your character:</p>

		<div id='button-container'>
			<button class="button buttonh" id="rhino" onclick="avatar(this.id)"><img src='../images/rhino.png' class="rhino"></button>
			<button class="button" id="giraffe" onclick="avatar(this.id)"><img src='../giraffe.png' class="giraffe"></img></button>
			<button class="button buttone" id="elephant" onclick="avatar(this.id)"><img src='../elephant.png' class="elephant"></button>
			<button class="button buttonh" id="hippo" onclick="avatar(this.id)"><img src='../hippo.png' class="hippo"></button>
			</div>

		</div>

		</div>


<?php

	//connect to database
    require('./connection.php');

	//Bounds of Gulu district
	$min_x = 32.12395;
	$max_x = 32.82805;
	$min_y = 2.4515;
	$max_y = 3.3066;

	//Generate random coordinates within Gulu district

	$lon = $min_y + ($max_y - $min_y) * (mt_rand() / mt_getrandmax());
	$lat = $min_x + ($max_x - $min_x) * (mt_rand() / mt_getrandmax());
	$point = pg_fetch_row(pg_query("SELECT ST_SetSRID( ST_Point('$lat', '$lon'), 4326)"))[0];

	//Get the grid of acholi
	$grid = pg_fetch_row(pg_query("SELECT wkb_geometry FROM grid WHERE ST_Intersects('$point',ST_Transform(wkb_geometry,4326))"))[0];
	$smallGrid = pg_query("SELECT id, ST_AsGeoJSON(ST_Transform(wkb_geometry,4326)) FROM grid_small WHERE ST_Contains('$grid',wkb_geometry)");

	$smallerGrid = '';
	$gridRow = '';


	//Create grid as a geojson
		while($row = pg_fetch_row($smallGrid)){


			$gridRow = (strlen($gridRow) > 0 ? ',' : '') . '{"type": "Feature", "id": ' . $row[0] . ', "properties": { "status": 0}, "geometry": ' . $row[1] . '}';

			$smallerGrid .= $gridRow;

			}

			$smallerGrid = '{"type": "FeatureCollection","name": "grid", "crs":{"type": "name", "properties": { "name": "urn:ogc:def:crs:OGC:1.3:CRS84" } }, "features": [ ' . $smallerGrid . ' ]}';
		$count = pg_num_rows($smallGrid);

		echo "<script>\n";
                                echo "let smallGrid = $smallerGrid\n";
				echo "let lat = $lat\n";
				echo "let lon = $lon\n";
				echo "let num = $count\n";
				echo "</script>\n";
?>
<script>
	

		var OsmRequest = require('osm-request');
		const osm = new OsmRequest({
                endpoint: 'https://api06.dev.openstreetmap.org',
                oauthConsumerKey: 'VnCASv0JrF4mqHQ3uWBCE6bf1dHFh14ltHoGBbmV',
                oauthSecret: '9fAkZCyRDkS8yAySv9MJYf78uFM8wcERG3oK1081',
                auto: true});


		//Add reference map
				map = L.map('map', {
                                        zoomControl: false,
                                        inertia: false,
                                        keyboard: false,
                                        dragging: false,
                                        scrollWheelZoom: false,
                                        attributionControl:false,
                                        zoomAnimation:false,
					maxZoom:17
                                });

                                //Load bing map for reference map
                                const bing = L.bingLayer('AgWUG-xtDgsFNlDhzIMjiuvn2sqdQ82E_ywrY3jp_mbrOiIgW9w7YJ39oJiKwteg', {
                                        maxZoom: 17}).addTo(map);
                

		//Add reference grid for overlay map
		refGrid = L.geoJson(smallGrid, {style: {color: '#000000', fillColor: '#000000', fillOpacity: 0}}).addTo(map);
		map.fitBounds(refGrid.getBounds(), {maxZoom: 17});

		//Go to mapping page when user chooses an avatar to play with 
		function avatar(animal) {


                                let f = document.createElement('form');
                                f.action = '../mapping.php';
                                f.method = 'POST';

                                let i = document.createElement('input');
                                i.type ='hidden';
                                i.name ='avatarChoice';
                                i.value = animal;
                                f.appendChild(i);
				

                                document.body.appendChild(f);
                                f.submit();


                        }


		

		</script>
	</body>

</html>
