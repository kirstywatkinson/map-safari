function makeMap(){
    
                //Check if no buildings - if not reload map 
				if(buildings == 'no buildings'){

					//Switch to a different square
					location.reload();
					

				} else {
                    
				//Create a layer containing the building shapefiles as a geoJSON
                buildingsLayer = L.geoJson(buildings, {
					style: {
                                        color: '#2bdfff',

                                        weight: 1,

                                        fillColor: '#2bdfff',

                                        fillOpacity: 0

                                        }});

				}

				//console.log(buildingsLayer);
    
                //initialise bounds variable
				let bounds; 

                //Do I need this? 
				document.getElementById("avatarSelection").style.display = 'none';

				
				//Editing not enabled when map first loads 
				editing = 'no';

				//Set move distance
				moveDistance = 40;

                //Put avatar in centre of map
                avatar = L.marker([centroid.coordinates[1], centroid.coordinates[0]], {icon: avatarIcon, rotationOrigin: 'center'}); 


				//set up map                                              
                //Create a variable containingthe map object and add the basemaps as layers
                map = L.map('map', {
                                        minZoom: 18,
                                        maxZoom: 20,
					maxBounds: [[miny,minx],[maxy,maxx]],
					inertia:false,
					editable: true
					});

				//Add small grid as leaflet geojson
                smallerGrid = L.geoJson(smallGrid);


				//Get location of avatar and find out what square it is in and then zoom map to that square
				let avatarLL = avatar.getLatLng();
				let pt = turf.point([avatarLL.lng, avatarLL.lat]);

				//Loop through each grid square
				smallerGrid.eachLayer(function(layer){

						//Get grid as a turf polygon
                        let poly = turf.polygon(layer.feature.geometry.coordinates);

						//Check if avatar intersects with grid
                        let ptIntersect = turf.booleanPointInPolygon(pt, poly);

						//Get bounds of the grid and zoom to it
                        if(ptIntersect === true){

							bounds = layer.getBounds();
							map.fitBounds(bounds, {maxZoom: 20});
						}

					});

				
				//Load bing and OpenStreetMap basemaps
				const bing = L.bingLayer('AgWUG-xtDgsFNlDhzIMjiuvn2sqdQ82E_ywrY3jp_mbrOiIgW9w7YJ39oJiKwteg', {
                                	maxZoom: 20
                                }).addTo(map);


				
				//Add OSM data to a GeoJSON
                osmLayer =  L.geoJson(osmGeo, {style: {
					color: '#f21b7d',

					weight: 1,

					fillColor: '#ff91c3',

					fillOpacity: 0.3

					}});
					
					
				//Initialise empty list to store iDs
				let iDs = [];
			
				//Loop through building layer and see if each building intersects with the OSM layer
				//If it does add its ID to the list of IDs
				buildingsLayer.eachLayer(function(layer1){
					
					
					osmLayer.eachLayer(function(layer2){

						let DB = turf.polygon(layer1.feature.geometry.coordinates[0]);

						let dbBuff = turf.buffer(DB, 1, {units:'meters'});

						let osmGeom = turf.polygon(layer2.feature.geometry.coordinates);

						let intersection = turf.booleanIntersects(osmGeom, dbBuff);
	
						if(intersection){

							iDs.push(layer1.feature.id);

						}

					})

					});

				//Remove buildings that intersect the OSM data based on IDs
				buildingsLayer.eachLayer(function(layer){

					for (i = 0; i < iDs.length; i++) { 

						if(layer.feature.id == iDs[i]){

							buildingsLayer.removeLayer(layer);

						}
					}

					
				})

				
				//Initialise empty list to store all building coordinates
				let coord = [];
				
				
				//Loop through buildings and get their coordinates so the original building coordinates are stored?
				buildingsLayer.eachLayer(function(layer){

					let building = layer.feature;

					let coords = [];

                    // Loop length of the data
                    for (let i = 0; i < building.geometry.coordinates[0][0].length; i++){


                           coords.push([building.geometry.coordinates[0][0][i][1],building.geometry.coordinates[0][0][i][0]])

                                        }

					coord.push(coords);

				});

                //Add buildings to maps
				buildingsLayer.addTo(map);

				//Ad empty geojson to display drawn buildings and set style
				drawnBuildings = L.geoJSON().addTo(map);
    
				drawnBuildings.setStyle({
                                        color: '#f21b7d',

                                        weight: 1,

                                        fillColor: '#ff91c3',

                                        fillOpacity: 0.3

                                        });				
				//Add OSM layer to map
				osmLayer.addTo(map);

				//Add the 9 grid squares to the map
				smallerGrid.addTo(map);
		

                //prevent map from zooming when double-clicking 
                map.doubleClickZoom.disable();

				//Prevent users dragging the map 
				map.dragging.disable();


				//Add avatar to map
				avatar.addTo(map);



				//Generate food
                food = L.layerGroup();
                makeFood(minx, maxx, miny, maxy, food);
                food.addTo(map);


				//Add reference grid for overlay map
				refGrid = L.geoJson(smallGrid, {style: {color: '#000000', fill: 'url(./Images/tile.jpg)', fillOpacity: 1, weight: 0.5}});

                //Get centroid location
				centroidLocation(smallerGrid, avatar);
				
				//Add reference map
				overlay = L.map('overlay', {
                                        zoomControl: false,
                                        inertia: false,
                                        keyboard: false,
                                        dragging: false,
                                        scrollWheelZoom: false,
                                        attributionControl:false,
                                        zoomAnimation:false,
					                    maxZoom:18
                                });


				//Add google basemap to overview map 
				const googleSat2 = L.tileLayer('http://{s}.google.com/vt/lyrs=s&x={x}&y={y}&z={z}',{
                                         maxZoom: 20,
                                            subdomains:['mt0','mt1','mt2','mt3']
                                }).addTo(overlay);


				//Set view of overview map to the entire grid
				overlay.fitBounds(smallerGrid.getBounds(), {maxZoom: 18});


				//Set overview map zoom based on screen size
				if (document.body.clientWidth < 1367 && document.body.clientWidth > 1099){
					//set reference map view
                    overlay.setZoom(13);

				} else if (document.body.clientWidth < 1100){
                    
                        //set reference map view
                        overlay.setZoom(12);


				} else {


				}


				//Add grid to reference map
				refGrid.addTo(overlay);

			
				//Add an event listener to the green button which when clicked uploads the selected building
                document.getElementById("training").addEventListener("click", function(event){
                    
                    document.getElementById("walkthroughStartPopUp").style.display = 'none';

				    walkthrough();

				});

				//Add an event listener to the green button which when clicked uploads the selected building
                document.getElementById("noTraining").addEventListener("click", function(event){

					document.getElementById("walkthroughStartPopUp").style.display = 'none';


				});

				//Add an event listener to the green button which when clicked uploads the selected building
				document.getElementById("uploadCreated").addEventListener("click", function(event){
                    
                                        //Remove upload button
                                        document.getElementById("uploadCreated").style.display='none';

                                        
                                        //Remove draw control
                                        map.removeControl(drawControl);
                    
                    

                                        //If there are drawn features on map
                                        if (drawnItems){
                                        
                                                //Get drawn layers
                                                let drawnLayers = drawnItems.getLayers();
                                                
                                                //Empty list to store geometries
                                                let geoms = [];


                                                //Upload to OSM
                                                uploadUser2OSM(drawnLayers, geoms)
                                                    .then((result)=>{
                                                    
                                                        //Add 1 to number of buildings mapped - this is for the finished pop up
                                                        buildingCount += 1;
                    					
							//Re-add avatar
                    					map.addLayer(avatar);

                                                        //Update score
                                                        let score = parseInt(document.getElementById("user_points").innerHTML);
                                                        document.getElementById("user_points").innerHTML = score + 1;
                                                    
                                                    
                                                        //Get element with computer score and update
                                                        let computerScore = parseInt(document.getElementById("machine_points").innerHTML);
                                                        document.getElementById("machine_points").innerHTML = computerScore - 1;

		                                        //Re-add add building button
                		                        document.getElementById("add").style.display='block';

                                                        //Play sound so user knows it's uploaded/they've gained points
                                                        pointSound.play();

                                          
                                        //Let player move 
                                        document.getElementById("up").disabled = false; 
                                        document.getElementById("down").disabled = false;
                                        document.getElementById("right").disabled = false;
                                        document.getElementById("left").disabled = false;
                                        document.getElementById("add").disabled = false;
          
                                                        //Loop through geometries
                                                        for(let i = 0; i <geoms.length; i++){

                                                                //Get tag
								                                tag = geoms[i][1];

                                                                //Get geometry
             	                                                geom = geoms[i][0];
								
                                                                //Upload to  database
								                                uploadUserGeom('./scripts/uploadUserGeom.php', tag, geom.geometry.coordinates[0], userName, function(data){

                                                                        console.log(data)

                                                                    })


							                             }                

							                             //Set the style of drawnBuildings so it matches osm layer
							                             drawnBuildings.setStyle({
                                                                		color: '#f21b7d',

                                                                		weight: 1,

                                                                		fillColor: '#ff91c3',

                                                                		fillOpacity: 0.3
										
                                                        });



							                         })
							                         }
                                                    
                                                    })

				//Add listener for when the upload button is pressed
				document.getElementById("upload").addEventListener("click", function(evt){

						//Prevent default click behaviour
						evt.preventDefault();

                        //Convert leaflet layer to geojson
						let building = activeBuilding.toGeoJSON();

						//0m buffer
						let buffer = turf.buffer(building,0,{units: 'meters'});

						//console.log(buffer.geometry.coordinates[0]);

                        //Get geometry of building
						let geom = buffer.geometry.coordinates[0];

						//If circle variable = true and area less than 50m2 tag = hut, else = yes
						if(circle === true){
							
							//Get area via turf
							let potentialHut = turf.polygon(building.geometry.coordinates);
							let hutArea = turf.area(potentialHut);

							//If less than 50 tag = hut
							if(hutArea < 50){
								tag = "hut";

							} else {

								tag = "yes";
							}
						} else {
							tag = "yes";
						};
						
						//Upload to database
						uploadMachineGeom('./scripts/uploadMachineGeom.php', id, tag, geom, userName, function(data){


							console.log(data);

						});
						
						//Add to drawn buildings layer
						drawnBuildings.addData(building);

						//Add 1 to building count this square
						buildingCount += 1;

						//Add to score
						let score = parseInt(document.getElementById("user_points").innerHTML);

			                        //Play sound
						pointSound.play();

                        			//Update score
                        			document.getElementById("user_points").innerHTML = score + 1;

                        			//Update computer score
						let computerScore = parseInt(document.getElementById("machine_points").innerHTML);
		                		document.getElementById("machine_points").innerHTML = computerScore - 1;

                        			//Set style of drawn buildings - do I need this???
						drawnBuildings.setStyle({
                                        		color: '#f21b7d',

                                        		weight: 1,

                                        		fillColor: '#ff91c3',

                                        		fillOpacity: 0.3

                                        	});

                                                //Remove building from the editable buildings layer
                                                map.removeLayer(activeBuilding);

                                                 //Disable edit buttons
                                                disableButtons();

                                                //Disable editing
                                                activeBuilding.transform.disable();


						//Upload to OSM function
                        upload(tag,geom)
                                    .then((result)=>{
                            
									//Turn off editing
									editing = 'no';

									//Set circle to false so next building not accidentally tagged as hut 
									circle = false;

									//Re-add add building button
									document.getElementById("add").style.display='block';						
									

									//Re-add avatar
									map.addLayer(avatar);


									//Get coords of avatat and set map view on the avatar at specified zoom level
									let avatarCoords = avatar.getLatLng();

									map.setView(avatarCoords, zoom, {animate: false,
														});


										//Stop player from moving 
									document.getElementById("up").disabled = false; 
									document.getElementById("down").disabled = false;
									document.getElementById("right").disabled = false;
									document.getElementById("left").disabled = false;
        	                              	
                        });


					});

					//Add event listener for when not sure is pressed
					document.getElementById("notSure").addEventListener("click", function(evt){
                        
					//Prevent default click behaviour
					evt.preventDefault();

					// Send ID to database via GET request to update database (user note sure)
					makeRequest(['./scripts/buildingUnsure.php?id=', id, '&user=', userName].join(''), function(data){
                            
                                                        console.log(data);
                                                });

						//Remove editable layer
			            activeBuilding.transform.disable();
						map.removeLayer(activeBuilding);

						//Disable editing buttons
                        disableButtons();

						//Re-add avatar
						map.addLayer(avatar);
						let avatarCoords = avatar.getLatLng();
                        
						//Re-add add building button
						document.getElementById("add").style.display='block';

						//Reset map view
						map.setView(avatarCoords, zoom, {animate: false,

                                                
                                                });
                        
                        //Turn off editing prompt
						editing = 'no';

                        //Re-enable movement
				        document.getElementById("up").disabled = false; 
		                        document.getElementById("down").disabled = false;
		                        document.getElementById("right").disabled = false;
        		                document.getElementById("left").disabled = false;

                                        });

						//Add event listener for when not a building is pressed 
					   document.getElementById("notBuilding").addEventListener("click", function(evt){
                           
                            // Send ID to database via GET request to update database (ready for deletion)
			        		makeRequest(['./scripts/markNotBuilding.php?id=', id, '&user=', userName].join(''), function(data){
									console.log(data);

			       		 	});

				            //Remove active building from map so no longer visible or editable
                            activeBuilding.transform.disable();
						    map.removeLayer(activeBuilding);
							   
				            //renable moving
                            document.getElementById("up").disabled = false; 
                            document.getElementById("down").disabled = false;
                            document.getElementById("right").disabled = false;
                            document.getElementById("left").disabled = false;
                           
							//Disable editing
                            disableButtons();

							//Re-add add building button
							document.getElementById("add").style.display='block';
                                            
							//Update scores
                            let score = parseInt(document.getElementById("user_points").innerHTML);    
                            document.getElementById("user_points").innerHTML = score + 1;
                           
                            let computerScore = parseInt(document.getElementById("machine_points").innerHTML);
                            document.getElementById("machine_points").innerHTML = computerScore - 1;
				
                            //Play points sound
                            pointSound.play();


				            //Re-add avatar
				            map.addLayer(avatar);
                            
                            //Reset map view
                            let avatarCoords = avatar.getLatLng();
                            map.setView(avatarCoords, zoom, {animate: false,

                                               
                        });
					

						//Disable editing 
						editing = 'no';
                                        });

				        //Add an event listener to the show building button to show building
                        document.getElementById("circularise").addEventListener("click", circularise);

					   //Keyboard shortcuts
					   window.addEventListener('keydown', function(e){
                           
                        //If key pressed is C and editing prompt is yes - circularise
						if(e.key == 'c' && editing == 'yes'){
							circularise(e);

						} else if(e.key == 'r' && editing == 'yes'){
							resetBuilding(e);


						}else if(e.key == 'h' && editing == 'yes'){

                            //Get hide button element
							let hide =  document.getElementById("hideOutline");

                            
                            //Check if hide is displayed 
							if(hide.style.display == 'block'){

									//Disable transform
									activeBuilding.transform.disable();					
								
			
                                    //Switch to show button
                                    let show = document.getElementById("showOutline");
                                    show.style.display='block';

                                    //Remove feature from map so it isn't visible to the user
                                    map.removeLayer(activeBuilding);

                                    //Hide the hide button
                                    let hide = document.getElementById("hideOutline");
                                    hide.style.display='none';



						} else {

							showOutline(e);

						}


						} 
						


					});

                    //Add an event listener to the hide button
                    document.getElementById("hideOutline").addEventListener("click", function(evt){
                        
                            //Prevent default click behaviour
                            evt.preventDefault();

                            //Disable editing
                            activeBuilding.transform.disable();

                            //Switch to show button
                            let show = document.getElementById("showOutline");
                            show.style.display='block';

                            //Remove feature from map so it isn't visible to the user
                            map.removeLayer(activeBuilding);

                            //Hide the hide button
                            let hide = document.getElementById("hideOutline");
                            hide.style.display='none';
                    });

                //add event listener for show outline button
                document.getElementById("showOutline").addEventListener("click", showOutline);


                //Enable user to move avatar using arrow keys		
				window.addEventListener('keydown', function(e) {
                                

					if(e.key == 'ArrowRight' && editing == 'no'){
						move('right');

					} else if (e.key == 'ArrowLeft' && editing == 'no'){
                                                move('left');

					} else if (e.key == 'ArrowUp' && editing == 'no'){
                                                move('up');

					} else if (e.key == 'ArrowDown'&& editing == 'no'){
						move('down');


					}


		                });
    
                //Reset map zoom level when map zoomed in and out

				map.on('zoomend', function(){

					let newZoom = map.getZoom();

					map.setZoom(newZoom);

				});


				
				




		}



		function addBuilding(){
		
			//Show upload button 
			document.getElementById("uploadCreated").style.display='block';

			//Remove add own building button 
			 document.getElementById("add").style.display='none';

			//Remove avatar
			map.removeLayer(avatar);

			//Create new feature group for drawn buildings to go in 
			drawnItems = new L.FeatureGroup();
			map.addLayer(drawnItems);


			//Stop player from moving 
            document.getElementById("up").disabled = true; 
            document.getElementById("down").disabled = true;
            document.getElementById("right").disabled = true;
            document.getElementById("left").disabled = true;
            document.getElementById("add").disabled = true;


            //If there is a draw control already on screen
			if(drawControl){

                //Remove draw control
				map.removeControl(drawControl);

			};

			//Add draw control for polygon to bottom right of map, rectangle and circle 
            drawControl = new L.Control.Draw({

                                        position: 'bottomright',
                                        edit: {marker: false,
                                                line: false,
                                                featureGroup: drawnItems,
                                                poly: {
                                                        allowIntersection: false
                                                        }
                                                },
                                        draw: {

                                                polygon: {
                                                        allowIntersection: false,
                                                        showArea: false
                                                        },
                                                circle:  {showRadius: false},
                                                rectangle: {showArea: false},
                                                polyline: false,
                                                marker: false,
                                                circlemarker: false
                                                }
                                });

			drawControl.setDrawingOptions({
                                         rectangle: {
                                                shapeOptions: {
                                                        color: '#FFFFFF'
                                                }
                                        },
                                        circle: {
                                                shapeOptions: {
                                                        color: '#FFFFFF'
                                                }
                                        },
                                        polygon: {
                                                shapeOptions: {
                                                        color: '#FFFFFF'
                                                }
                                        }

                                });

                        map.addControl(drawControl);


			//When item created add to drawn items
			map.on(L.Draw.Event.CREATED, function (e) {
                                        layer = e.layer;

                                        let type = e.layerType

                                        drawnItems.addLayer(layer);

			});
			
			//When edited
			map.on('draw:edited', function (e) {
                                         var layers = e.layers;
                                        layers.eachLayer(function (layer) {

                                         });
                                });
			

		}

		/* FUNCTION to find location of avatar and unlocked squares if appropriate */
		function centroidLocation(data, avatar){


				//Get location of avatar and convert to turf point
				let avatarLL = avatar.getLatLng();
				let pt = turf.point([avatarLL.lng, avatarLL.lat]);

				//Set squares unlocked as 0 
				let squaresUnlocked = 0;
				
				
				//Loop through grid square
				data.eachLayer(function(layer){

				//Create turf polygon from square coordinates
				let poly = turf.polygon(layer.feature.geometry.coordinates);


				//See if avatar and square intersect
                let ptIntersect = turf.booleanPointInPolygon(pt, poly);


				//If yes set status as 1
				if(ptIntersect === true){

                            layer.feature.properties.status = 1;
							
							//Loop through ref grid and set status as 1 if it matches the layer id and set style

							refGrid.eachLayer(function(layer1){
			
								if(layer1.feature.id == layer.feature.id){
									
									layer1.feature.properties.status = 1;

									layer1.setStyle({color: '#000000',

                                                                                weight: 1,

                                                                                fillColor: '#000000',

                                                                                fillOpacity: 0})

								} 


							})


                            } else{

                                                }

						//If status = 1 set style and add 1 to squares unlocked
						if (layer.feature.properties.status === 1){
							layer.setStyle({
                                                                                color: '#000000',

                                                                                weight: 0,

                                                                                fillColor: '#000000',

                                                                                fillOpacity: 0

		
                                                        });

						squaresUnlocked += 1;



						} else {

							layer.setStyle({
                                                                                color: '#000000',

                                                                                weight: 1,

                                                                                fill: 'url(./Images/tile.jpg)',

                                                                                fillOpacity: 1

                                                        });

						}


                                });

			//if all squares unlocked finished button appears
			//Not in use for research so players can stop playing at end of 15 mins

			if (squaresUnlocked === 9){


				document.getElementById('finished').style.display = 'inline-block';

			}



			}


		function move(id){

			let coords;
	
			if(id == 'up'){
				
				//Get coords of avatar as turf point
				let Lcoords = avatar.getLatLng();
				let point = turf.point([Lcoords.lng, Lcoords.lat]);
                
                //Work out next coordinate 
				let nextCoord = turf.destination(point,moveDistance*1.25, 0, {units: 'meters'});
				
				//If not going to go out of bounds 
				if(parseFloat(nextCoord.geometry.coordinates[1]) < parseFloat(maxy)){

                //Get new coordinate    
				let newCoord = turf.destination(point, moveDistance, 0, {units: 'meters'});

                //Set avatar coords as the new coordinates
				coords = L.latLng(newCoord.geometry.coordinates[1], newCoord.geometry.coordinates[0]);
				avatar.setLatLng([newCoord.geometry.coordinates[1], newCoord.geometry.coordinates[0]]);
				
                //Set avatar icon and rotation
				avatar.setIcon(avatarIcon);
				avatar.setRotationAngle(275);
				
				//check avatar location relative to grid
				centroidLocation(smallerGrid, avatar);
			
				//Check to see if it is close to food
				checkForFood(avatar);
				
				//Check for nearby buildings
				checkForBuilding(avatar);

				//Lose energy
				energy.style.width = parseFloat(energy.style.width)-1 + "%";

				//Check how much energy 
				checkEnergy(energy);

				} else {
				
				//if at boundary  don't move avatar

				    coords  = avatar.getLatLng();
				    avatar.setRotationAngle(0);

                    centroidLocation(smallerGrid, avatar);


                    //Game checks
                    checkForFood(avatar);
                    checkForBuilding(avatar);
				    checkEnergy(energy);

				}

				
			} else if(id == 'down'){

                //Get coords of avatar as turf point
				let Lcoords = avatar.getLatLng();
				let point = turf.point([Lcoords.lng, Lcoords.lat]);

                //Work out new coordinate
				let nextCoord = turf.destination(point,moveDistance*1.25, 180, {units: 'meters'});

                //If not out of bounds
				if(parseFloat(nextCoord.geometry.coordinates[1]) > parseFloat(miny)){

                        //Set avatar coordinates
                        let newCoord = turf.destination(point, moveDistance, 180, {units: 'meters'});
                        coords = L.latLng(newCoord.geometry.coordinates[1], newCoord.geometry.coordinates[0]);
                        avatar.setLatLng([newCoord.geometry.coordinates[1], newCoord.geometry.coordinates[0]]);
                    
                        //Set avatar icon and rotation
				        avatar.setIcon(avatarIcon);
                        avatar.setRotationAngle(90);


                        //Check location of avatar to small grid
                        centroidLocation(smallerGrid, avatar);

                        //Game checks
                        checkForFood(avatar);
                        checkForBuilding(avatar);


                        //Lose energy for moving
                        energy.style.width = parseFloat(energy.style.width)-1 + "%";
                        checkEnergy(energy);


				} else {

                        //Don't move avatar
                        coords  = avatar.getLatLng();

                        //Set avatar icon and rotation
                        avatar.setIcon(avatarIcon);
                        avatar.setRotationAngle(0);

                        //Check location of avatar relative to grid
                        centroidLocation(smallerGrid, avatar);

                        //Game checks
                        checkForFood(avatar);
                        checkForBuilding(avatar);
                        checkEnergy(energy);

				}


			} else if(id == 'left'){
                
                
                //Get coords of avatar as turf point
				let Lcoords = avatar.getLatLng();
				let point = turf.point([Lcoords.lng, Lcoords.lat]);
                
                //Work out next coordinate 
				let nextCoord = turf.destination(point, moveDistance*1.25, 270, {units: 'meters'});
                
                //If not out of bounds
				if(parseFloat(nextCoord.geometry.coordinates[0]) > parseFloat(minx)){
                    
                    
                        //Set new avatar coords
                        let newCoord = turf.destination(point, moveDistance, 270, {units: 'meters'});
				        coords = L.latLng(newCoord.geometry.coordinates[1], newCoord.geometry.coordinates[0]);
				        avatar.setLatLng([newCoord.geometry.coordinates[1], newCoord.geometry.coordinates[0]]);

				        //Set avatar icon and rotation
                        avatar.setIcon(avatarIconFlip);
                        avatar.setRotationAngle(0);
                        
                        //Check avatar location relative to grid
                        centroidLocation(smallerGrid, avatar);

				
                        //Game checks
                        checkForFood(avatar);
				        checkForBuilding(avatar);

                        //Lose energy 
				        energy.style.width = parseFloat(energy.style.width)-1 + "%";
				        checkEnergy(energy);

				} else {
                    
                    //Don't move avatar
				    coords  = avatar.getLatLng();
                    
                    //Set avatar icon and rotation
                    avatar.setIcon(avatarIcon);
                    avatar.setRotationAngle(0);

                    //Check avatar location on grid
                    centroidLocation(smallerGrid, avatar);


                    //Game checks
                    checkForFood(avatar);
                    checkForBuilding(avatar);
                    checkEnergy(energy);


				}

			} else {

                //Get coords of avatar as turf point
                let Lcoords = avatar.getLatLng();
                let point = turf.point([Lcoords.lng, Lcoords.lat]);

                //Work out next coordinate 
				let nextCoord = turf.destination(point, moveDistance*1.25, 90, {units: 'meters'});
                
                
                //If not out of bounds
				if(parseFloat(nextCoord.geometry.coordinates[0]) < parseFloat(maxx)){
                    
                    //Set new avatar coords
                    let newCoord = turf.destination(point, moveDistance, 90, {units: 'meters'});
                    coords = L.latLng(newCoord.geometry.coordinates[1], newCoord.geometry.coordinates[0]);
                    avatar.setLatLng([newCoord.geometry.coordinates[1], newCoord.geometry.coordinates[0]]);

                    //Set avatar icon and rotation
				    avatar.setIcon(avatarIcon);
				    avatar.setRotationAngle(0);

                    //Check avatar location relative to grid
				    centroidLocation(smallerGrid, avatar);

                    //Game checks
                    checkForFood(avatar);
                    checkForBuilding(avatar);
                    
                    //Lose energy
				    energy.style.width = parseFloat(energy.style.width)-1 + "%";
                    checkEnergy(energy);


				} else {

                    //Don't move avatar
				    coords  = avatar.getLatLng();

                    //Set avatar icon and rotation
                    avatar.setIcon(avatarIcon);
                    avatar.setRotationAngle(0);

                    //Check avatar location on grid
                    centroidLocation(smallerGrid, avatar);


                    //Game checks
                    checkForFood(avatar);
                    checkForBuilding(avatar);
                    checkEnergy(energy);




				}

			}
			
            //Set map view to new coords

            map.setView(coords, zoom, {animate: false,



            });



		}

		function checkForFood(avatar){

			 //Get avatar location 
             let avatarLL = avatar.getLatLng();
             let pt = turf.point([avatarLL.lng, avatarLL.lat]);


			 //Loop through food layer
			 food.eachLayer(function(layer){

				//Add buffer to food point and check if it intersects with the avatar location 
				let foodPt = turf.point([layer.getLatLng().lng, layer.getLatLng().lat]);

				let buffer = turf.buffer(foodPt, moveDistance, {units: 'meters'});

                let intersect = turf.booleanIntersects(pt,buffer);

                //If it intersects
                if(intersect){

                    //Play eating sound
                    leafSound.play();

                    //Remove the food from map 
                    food.removeLayer(layer);

                    //Add energy 
                    energy.style.width = (parseFloat(energy.style.width) + 25)  + "%";


				} 
				

			});

		}

		function checkEnergy(energy){


			//If energy less than 25 turn bar amber
			if(parseFloat(energy.style.width) > 25) {

				energy.style.backgroundColor = "rgb(4, 170, 109)";

			}
			
			//if energy less than 25 turn bar red 
			else if(parseFloat(energy.style.width) < 25) {


			     energy.style.backgroundColor = "rgb(255, 166, 0)";

			} 
			//If energy = 0 
			if(parseFloat(energy.style.width) === 0 ){

				//Calculate random amount of points the machine will recieve between 0 and 6 

				let machinePoints = Math.floor(Math.random() * 6);

				document.getElementById("machine-points").innerHTML = machinePoints;

				//Bar color grey
				energy.style.backgroundColor = "rgb(255, 60, 0)";

				//get a random quote 
				makeRequest(['./scripts/chooseQuote.php?points=', machinePoints, '&user=', userName].join(''), function(data){

						document.getElementById("popUp-message").innerHTML = data;

                                                });

				//Stop player from moving 
				document.getElementById("up").disabled = true; 
				document.getElementById("down").disabled = true;
				document.getElementById("right").disabled = true;
				document.getElementById("left").disabled = true;
				document.getElementById("add").disabled = true;

				//Open pop up 
				energyPopUp();

			} 

		}


function checkForBuilding(avatar){

			
            //Get avatar location as a point
            let avatarLL = avatar.getLatLng();
            let pt = turf.point([avatarLL.lng, avatarLL.lat]);


		    let buildingPossible;    
            let possibleBuildings = [];

            //iterate through buildings layer
			buildingsLayer.eachLayer(function(layer){


                //Check for intersection between building and the buffered avatar location
				let poly = turf.polygon(layer.feature.geometry.coordinates[0]);
				let buffer = turf.buffer(poly, moveDistance, {units: 'meters'});
				let intersect = turf.booleanIntersects(pt,buffer);


			     if(intersect){

					//Stop player from moving 
					document.getElementById("up").disabled = true; 
					document.getElementById("down").disabled = true;
					document.getElementById("right").disabled = true;
					document.getElementById("left").disabled = true;


					//Re-add add building button
					document.getElementById("add").style.display='none';

			         //get feature from layer
					buildingPossible = layer.feature;                

					//Stop looping
					return false; 

				} 

    
			});

            //If there is a building
		   if(buildingPossible){
	
                    
                    //Remove avatar
                    map.removeLayer(avatar);
               
				    //Set editing prompt as yes so keyboard shortcuts work
					editing = 'yes';
               
					//Show editing buttons
                    document.getElementById("upload").style.display = 'block';
					document.getElementById("notSure").style.display = 'block';
					document.getElementById("notBuilding").style.display = 'block';
					document.getElementById("circularise").style.display = 'block';
					document.getElementById("reset").style.display = 'block';
					document.getElementById("hideOutline").style.display = 'block';

					//Initialise tag variable
                    let tag;

                    //get id of feature (required for upload to database)
					let id = buildingPossible.id;

                    
					//Get coordinates of the feature so it can be added as a leaflet polygon
                    //So it works with the editing libraries
                    coords = [];
					// Loop length of the data
                	for (let i = 0; i < buildingPossible.geometry.coordinates[0][0].length; i++){

                                  //push feature coordinates
						          coords.push([buildingPossible.geometry.coordinates[0][0][i][1],buildingPossible.geometry.coordinates[0][0][i][0]])



					}

					//console.log(coords);

                    //Create feature as a polygon
					activeBuilding = L.polygon(coords, {transform: true, draggable:true}).addTo(map);

                    //Remove original building layer
					buildingsLayer.eachLayer(function(layer){


							//Check for intersection between building and the buffered avatar location
							let poly = turf.polygon(layer.feature.geometry.coordinates[0]);
							let buffer = turf.polygon(buildingPossible.geometry.coordinates[0]);
							let intersect2 = turf.booleanIntersects(poly,buffer);


						    if(intersect2){
							
							     //Remove from buildings layer geojson
                                 buildingsLayer.removeLayer(layer);

							
								//Remove layer from map and buildingsLayer so it can't be seen
								map.removeLayer(layer);

								//Backup
								backUp = layer;

                				}


						})    


                    //Set style
					activeBuilding.setStyle({
											color: '#00ffb3',

											weight: 1.5,

											fillColor: '#00ffb3',

											fillOpacity: 0

					});

					//Enable editing on feature
					activeBuilding.transform.enable({boundsOptions:{color: 'black'}, rotateHandleOptions:{color:'black'}, uniformScaling: true, dragging:true});

					//Event listener for reset button 
					document.getElementById("reset").addEventListener("click", resetBuilding);


                    //In case of error
					map.addEventListener("error", function(event) {

                        
					//Disable editing
						activeBuilding.transform.disable();

					//Remove feature from map so it isn't visible to the user
					map.removeLayer(activeBuilding);

				
					//Remove feature from map so it isn't visible to the user
					map.addLayer(activeBuilding);
                        
					//Enable editing on feature
					activeBuilding.transform.enable({boundsOptions:{color: 'black'}, rotateHandleOptions:{color:'black'}, uniformScaling: true, dragging:true});

						 })

				
					} else {

					}


		}

	/**
	* FUNCTION: Disable  editing buttons
	*/


	function disableButtons(){

					document.getElementById("upload").style.display = 'none';
                    document.getElementById("notSure").style.display = 'none';
                    document.getElementById("notBuilding").style.display = 'none';
                    document.getElementById("circularise").style.display = 'none';
                    document.getElementById("reset").style.display = 'none';
					document.getElementById("hideOutline").style.display = 'none';

	}


	function showOutline(event){
        
        event.preventDefault();

        //Switch to hide button
        let hide = document.getElementById("hideOutline");
        hide.style.display='block';

        //Add feature back to map so it is visible to the user
        map.addLayer(activeBuilding);

        //Enable path transform (scale, rotate and drag)
        activeBuilding.transform.enable({boundsOptions:{color: 'black'}, rotateHandleOptions:{color:'black'}, uniformScaling: true});

        //Hide the show button
        let show = document.getElementById("showOutline");
        show.style.display='none';

	}

		

		/**
        * Make a request for JSON over HTTP, pass resulting text to callback when ready
        */
        function uploadUserGeom(url, tag, geom, user, callback) {


                // create request instance
                  let request = new XMLHttpRequest();

                  // initialise request withURL
                  request.open('POST', url);

                  // set listener for when the response comes from the server
                  request.onreadystatechange = function () {

                    // check the response is OK
                    if (this.readyState === 4) {

                      // just print out some information
                      console.log('Status:', this.status);
                      console.log('Headers:', this.getAllResponseHeaders());
                      console.log('Body:', this.responseText);

                      callback(JSON.parse(this.responseText));

                    }
                  };


                // set the data you want to pass to the server as a JSON String
                const body = '{"tag": "' + tag.toString() + '"' + ',"user": "' + user.toString() + '"' + ',"geom": {"type":"MultiPolygon","coordinates":[[' + JSON.stringify(geom) + ']]}' + '}';

                // send the request
                request.send(body);


        }


	function resetBuilding(event){

			event.preventDefault();

			//Set coordinates to original coordinates
			activeBuilding.setLatLngs(coords);


			//Reset transform handlers
			activeBuilding.transform.reset();

			//Disable transform and then reenable
			activeBuilding.transform.disable;
			activeBuilding.transform.enable({boundsOptions:{color: 'black'}, rotateHandleOptions:{color:'black'}, 		uniformScaling: true});


	}

	    /**
        * Make a request for JSON over HTTP, pass resulting text to callback when ready
        */
        function uploadMachineGeom(url,id, tag, geom, user, callback) {


                // create request instance
				let request = new XMLHttpRequest();

				// initialise request withURL
				request.open('POST', url);

				// set headers (that you are sending JSON)
				//request.setRequestHeader('Content-Type', 'application/json');

				// set listener for when the response comes from the server
				request.onreadystatechange = function () {

				// check the response is OK
				if (this.readyState === 4) {

					// just print out some information
					console.log('Status:', this.status);
					console.log('Headers:', this.getAllResponseHeaders());
					console.log('Body:', this.responseText);

					callback(JSON.parse(this.responseText));

                    }
                  };


		          //the data you want to pass to the server as a JSON String
                const body = '{"id":' + id.toString() + ',"tag": "' + tag.toString() + '"' + ',"user": "' + user.toString() + '"' + ',"geom": {"type":"MultiPolygon","coordinates":[[' + JSON.stringify(geom) + ']]}' + '}';


                // send the request
                request.send(body);
                console.log(body);

        }





		/**
		 * Make a request for JSON over HTTP, pass resulting text to callback when ready
		 */
	
		function makeRequest(url, callback) {

			//initialise the XMLHttpRequest object
			var httpRequest = new XMLHttpRequest();

			//set an event listener for when the HTTP state changes
			httpRequest.onreadystatechange = function () {
		
			//a successful HTTP request returns a state of DONE and a status of 200
			if (httpRequest.readyState === XMLHttpRequest.DONE && httpRequest.status === 200) {
					//Parse returned json
	
					callback(JSON.parse(httpRequest.responseText));
				}
			};

			//prepare and send the request
			httpRequest.open('GET', url);
			httpRequest.send();
		}


		/**
        * Make a request for JSON over HTTP, pass resulting text to callback when ready
        * Returns a promise
	   */
        function makeRequestPromise(url) {

		return new Promise(function (resolve, reject) {

                	//initialise the XMLHttpRequest object
                	var httpRequest = new XMLHttpRequest();

                	//set an event listener for when the HTTP state changes
                	httpRequest.onreadystatechange = function () {
                
					//a successful HTTP request returns a state of DONE and a status of 200
					if (httpRequest.readyState === XMLHttpRequest.DONE && httpRequest.status === 200) {
                                        
					//Parse returned json
					resolve(JSON.parse(httpRequest.responseText));
                        	}
                	};

                	//prepare and send the request
                	httpRequest.open('GET', url);
                	httpRequest.send();

		})
        }


	


function makePOSTRequest2(url,xml,lat, lon) {


		return new Promise(function (resolve, reject) {

			// create request instance
			let request = new XMLHttpRequest();

			// initialise request withURL
			request.open('POST', url);

			// set headers (that you are sending JSON)
			request.setRequestHeader('Content-Type', 'application/json');

			// set listener for when the response comes from the server
			request.onreadystatechange = function () {

				if (this.readyState === 4) {

					// just print out some information
					console.log('Status:', this.status);
					console.log('Body:', this.responseText);

						//If resolved parse the json sent back by the request
						resolve(this.responseText);
				}

				//need to catch errors

				}; 

		const body = '{"lat":' + lat.toString() + ',"lon":' + lon.toString() + ',"xml":' + JSON.stringify(xml[1]) + '}';

		// send the request
		request.send(body);

	});

}

/** 
* Async function that uploads the geometry to OSM as a changeset
*/

async function upload(tag,geom){
			
		console.log(geom);


		//Initialise OSM element variable
		let newElement;		
		
		//Set up some arrays to construct the feature changeset xml
		nodeIds = [];
		nodes = [];
		element = [];

		//Open a changeset 
		let changesetId = await osm.createChangeset('Map Safari', 'Added building', {source: 'Microsoft Building Footprints', imagery_used: 'Bing aerial imagery'});

		//Create OSM nodes with the coordinates of the feature. Set timestamp of creation and version number (1)
		for(i = 0; i < geom.length-1; i++){
			let node = osm.createNodeElement(geom[i][1],geom[i][0]);
                        node = osm.setTimestampToNow(node);
                        node = osm.setVersion(node, 1);

			//Push nodes to a nodes array
			nodes.push(node);


			//Create an OSM element with the node and changesetId and return an node id
			newElement = await osm.sendElement(node, changesetId);
                        
			//Add element to element array
			element.push(newElement);

			//Add returned node Id to an array
                        nodeIds.push('node/' + newElement);
		}

		for(i = 0; i < geom.length; i++){
			console.log(geom[i][1],geom[i][0]);

		}

		
		//Create the changeset tag - input will what the user chose (hut or  yes)
		let tags = {building: tag, source: 'Microsoft Building Footprints' }


		//Add the first node as the last node so that the feature will make a polygon
		nodeIds.push('node/' + element[0]);
		
		//Create a way out of the nodes, set its timestamp as current time and version as 1 
		let way = await osm.createWayElement(nodeIds, tags);
		way = osm.setTimestampToNow(way);
		way = osm.setVersion(way, 1);
		



		//send element to OSM - send the nodes and the ways
        const newWay = await osm.sendElement(way, changesetId);
		
		//Close the changeset
		let close = await osm.closeChangeset(changesetId);		
		changeset = changesetId;
        
		return changesetId
		
	}

/**
* FUNCTION: Upload user created geometries 
*/

async function uploadUser2OSM (drawnLayers, geoms){

//Initialise some variables
let geom;
let layer;

//Loop through drawnLayers item 

for (let index = 0; index < drawnLayers.length; index++){

	//Get individual layer
    layer = drawnLayers[index];

    let tag;
	
    //If has property _mRadius - which means it is a circle so possibly be tagged as a hut
    if(layer._mRadius){

        // get centroid
		centroid = turf.point([layer._latlng.lng, layer._latlng.lat]);

		//get radius
		radius = layer._mRadius;

		//buffer using turf
		circle = turf.buffer(centroid, radius, {units: 'meters'});

        //Add to drawnBuildings layer so it shows as an uneditable layer on map
        drawnBuildings.addData(circle);

	//Calculare area of the circle
	let circleArea = turf.area(circle);
	
	//If less than 50m2 tag as hut else building
	if(circleArea < 50){

        //set tag as hut
        tag = 'hut';

	} else {

		tag = 'yes';
	}

		//geo variable = circle 
        geom = circle;

	//Add the geometry and its tag to the geoms list
	geoms.push([geom, tag]);
	
	//Remove layer from the drawnItems layer
	drawnItems.removeLayer(layer);	

        } else {
            
            //get coordinates of drawn feature
            let coords = layer._latlngs;

            //Convert to geoJSON
            building2 = L.polygon(coords);
            let bG = building2.toGeoJSON();

            //Add to drawn buildings layer
            drawnBuildings.addData(bG);
            
            //Set tag as yes e.g. building = yes
            tag = 'yes';

            //Set geometry as the json version of the building
            geom = bG;

	       //Add geometry and tag to geoms list
	       geoms.push([geom, tag]);

            //Remove layer from the drawnItems layer
            drawnItems.removeLayer(layer);	
                }
                                
        //Initialise OSM element variable
		let newElement;		
		
		//Set up some arrays to construct the feature changeset xml
		nodeIds = [];
		nodes = [];
		element = [];

		//Open a changeset 
		let changesetId = await osm.createChangeset('Map Safari', 'Added building', {source: 'aerial imagery', imagery_used: 'Bing aerial imagery'} );

		//Create OSM nodes with the coordinates of the feature. Set timestamp of creation and version number (1)
		for(i = 0; i < geom.geometry.coordinates[0].length-1; i++){

				let node = osm.createNodeElement(geom.geometry.coordinates[0][i][1],geom.geometry.coordinates[0][i][0]);
				node = osm.setTimestampToNow(node);
				node = osm.setVersion(node, 1);


				//Push nodes to a nodes array
				nodes.push(node);

				//Create an OSM element with the node and changesetId and return an node id
				newElement = await osm.sendElement(node, changesetId);
								
				//Add element to element array
				element.push(newElement);

				//Add returned node Id to an array
				nodeIds.push('node/' + newElement);

		}
		
		//Create the changeset tag - input will what the user chose (hut or  yes)
		let tags = {building: tag, source: 'aerial imagery' }
		
		//Add the first node as the last node so that the feature will make a polygon
		nodeIds.push('node/' + element[0]);	
	
		//Create a way out of the nodes, set its timestamp as current time and version as 1 
		let way = await osm.createWayElement(nodeIds, tags);
		way = osm.setTimestampToNow(way);
		way = osm.setVersion(way, 1);
		
		//send element to OSM - send the nodes and the ways
        const newWay = await osm.sendElement(way, changesetId);
		
		//Close the changeset
		let close = await osm.closeChangeset(changesetId);				
 
}
        
}

	function circularise(event){

			event.preventDefault();

			//Circularise  polygon function
			circularisePoly(activeBuilding, map);

			//Reset transform handlers
			activeBuilding.transform.reset();

	}




/**
	* Function to circularise  polygons once option selected from menu
	*
	*/ 
		
	function circularisePoly(layer, map){

		//Set circle variable as true 
		circle = true;

		//get original coordinates
		orig  = layer.toGeoJSON();
	
        console.log(orig);

	    //get centroid of the right clicked feature
		let centroid = turf.centroid(orig);

		//set up an array to store the distances between the centroid and the vertices
		let distances = [];

                
		// Loop length of the data
        for (let i = 0; i < orig.geometry.coordinates[0].length; i++){
                        

			//Get coordinate pair of the vertex
			let coordinatePair = turf.point(orig.geometry.coordinates[0][i]);

			//work out distance between vertex and the centroid
			let distance = turf.distance(centroid, coordinatePair, {units: 'kilometres'});

			//store in the distance array
			distances.push(distance)
		
             }
	
			//find out max distance and store as a variable
			let maxDist = Math.max.apply(null, distances);

			let rad = maxDist * 1000
		
			// Multiply max distance by sqrt of 2
			let buffDist = maxDist / Math.sqrt(2);

			//buffer from centroid.
			let buffered = turf.buffer(centroid, buffDist, {units: 'kilometres'});
		
		//Get new coordinates of the feature - in leaflet style LatLong
		newCoords = [];
		for (let i = 0; i < buffered.geometry.coordinates[0].length; i++){
			newCoords.push([buffered.geometry.coordinates[0][i][1], buffered.geometry.coordinates[0][i][0]]);
			//console.log(buffered.geometry.coordinates[0][i][0]);
		};

		//Update coordinates of polygon layer
		activeBuilding.setLatLngs(newCoords);
		
		return circle;

		
        }

/**
* FUNCTION: create food at start 
*/
function makeFood(minx, maxx, miny, maxy, layer){

    //Set icon of food

    leafIcon = L.icon({
                        iconUrl: './Images/leaf.png',
                        iconSize:     [40, 40], // size of the icon
                        iconAnchor:   [0, 0], // point of the icon which will correspond to marker's location

                });
    
    //Generate random coordinates
    for (i = 0; i < 40; i++){

        //Generate random coordinates within the grid
        let lon = miny + (Math.random() * (maxy - miny));
        let lat = minx + (Math.random() * (maxx - minx));


        //Create a marker from the random points
        let feature = L.marker([lon,lat], {icon: leafIcon});

        //Add to layer which will display as food on map 
        layer.addLayer(feature);

	}



return layer;

}


function energyPopUp(){

        //Play tired sound
		tiredSound.play();
    
        //Create popup and set text
        let energyPopUp = document.getElementById("energyPopUp");
        energyPopUp.style.display = 'block';

		
		//Event listener when okay button pressed
        document.getElementById("okay").addEventListener("click", function(evt){
		
        //Prevent default click behaviour
        evt.preventDefault();
            
        //Hide the pop up
        energyPopUp.style.display = "none";
			
			
        //Allow player to move again 
        document.getElementById("up").disabled = false;
        document.getElementById("down").disabled = false;
        document.getElementById("right").disabled = false;
        document.getElementById("left").disabled = false;
        document.getElementById("add").disabled = false;

        //Add energy 
        energy.style.width = (parseFloat(energy.style.width) + 25) + "%";

        //Set to amber?
        energy.style.backgroundColor = "rgb(255, 166, 0)";

        //Check energy 
        checkEnergy(energy);


		});


        }

function finished(){


		//Set building count in pop up
		document.getElementById("buildingsUploaded").innerHTML = buildingCount;

		//Create popup and set text
        let finishedPopUp = document.getElementById("finishedPopUp");
				
        //Show pop up 
        finishedPopUp.style.display = 'block';


        //Event listener when okay button pressed
        document.getElementById("complete").addEventListener("click", function(evt){
                        
        //Prevent default click behaviour
        evt.preventDefault();
                        
        // Send ID to database via GET request to update database (grid finished)
        makeRequest(['./scripts/gridComplete.php?id=', id, '&user=', userName].join(''), function(data){
                                                        console.log(data);
                        });
						

        //Pop up content
        document.getElementById("finished-message").innerHTML = 'Awesome job! Do you want to explore the next square and gain more experience? <br><span><button style="background-color:#4CAF50; width: auto; height:auto" id="carryOn">More exploring!</button><button style="background-color:#fff080; width: auto; height: auto; margin-left: 2%" id="stopMapping">Time to stop for now!</button></span>'
        document.getElementById("finishedPopUp").style.height = '15%';


                });


		
		//Event listener when okay button pressed
        document.getElementById("givenUp").addEventListener("click", function(evt){
                        
		//Prevent default click behaviour
		evt.preventDefault();


		//set status of square to check again

		// Send ID to database via GET request to update database (user note sure)
		makeRequest(['./scripts/gridPartial.php?id=', id, '&user=', userName].join(''), function(data){
										console.log(data);
		});

			document.getElementById("finished-message").innerHTML = 'Phew mapping is tiring! Do you want to try exploring the next square or take a rest by the Nile? <br><span><button style="background-color:#4CAF50; width: auto; height:auto" id="carryOn">More exploring!</button><button style="background-color:#fff080; width: auto; height: auto; margin-left: 2%" id="stopMapping">Time for a rest!</button></span>'
            
			//Add conditional statement based on screen size?
			document.getElementById("finishedPopUp").style.height = '15%';


                });

			//Add event listeners
            document.addEventListener("click", carryOn);
            document.addEventListener("click", stopMapping);



}

function carryOn(event){

		//If the player wants to continue refresh page
		if(event.target.id === 'carryOn'){

			//Switch to a different square
			location.reload();

		}

}

function stopMapping(event){

		if(event.target.id === 'stopMapping'){
				
			        //go to homepage
					let f = document.createElement('form');
						f.action = getNextPath();
						f.method = 'POST';

						let i = document.createElement('input');
						i.type ='hidden';
						i.name ='user';
						i.value = userName;
						f.appendChild(i);

						document.body.appendChild(f);
						f.submit();

                }

}


/** 
* FUNCTION: Initialise walkthrough
*/

function walkthrough(){

	//Say that the user has completed the walkthrough 

	makeRequest(['./scripts/walkthroughComplete.php?user=' , userName].join(''), function(data){
			console.log(data);

		});
		
	//Set video count as 0
	videoCount = 0;
		
	//Remove the grid that hides the other squares from view
    map.removeLayer(smallerGrid);

	document.getElementById("add").style.display = 'none';

	//Open pop up that will guide the user with first instruction
    document.getElementById("walkthroughHTML").innerHTML = "<b>Apowyo bino!</b> I'm " + name + " and I'll be guiding you through the area. To make me move you need to use the arrow keys on your keyboard or the buttons on the right of the screen. Have a go!";
	document.getElementById("walkthrough").style.display = 'block';

	
	//Change style depending on screen size 
	if (document.body.clientWidth < 1367){

		//Change height of pop up box
		document.getElementById("walkthrough").style.fontSize = '12px';

        }



	//Highlight the buttons for the user
	document.getElementById("up").style.boxShadow = '0 0px 15px #00ffff';
	document.getElementById("down").style.boxShadow = '0 0px 15px #00ffff';
	document.getElementById("left").style.boxShadow = '0 0px 15px #00ffff';
	document.getElementById("right").style.boxShadow = '0 0px 15px #00ffff';


    //Event listener for when they click a move button
	document.getElementById("arrows").addEventListener("click", stepOne);
	
	document.addEventListener("click", stepTwo);

	document.addEventListener("click", stepThree);	
	
	document.addEventListener("click", showOutlineDemo);

	document.addEventListener("click", stepFour);

	document.addEventListener("click", stepFive);

	document.addEventListener("click", stepSeven);

	document.addEventListener("click", stepEight);

	document.addEventListener("click", stepNine);

	//Add event listener for add own button
        document.getElementById("addDemo").addEventListener("click", addBuildingDemo);

	document.addEventListener("click", stepTen);

	document.addEventListener("click", stepEleven);

	document.addEventListener("click", stepTwelve);

	document.addEventListener("click", stepThirteen);

	document.addEventListener("click", stepFourteen);

	document.addEventListener("click", finalStep);


}


/**
* FUNCTION: Play sounds 
*/

function sound(src) {
  this.sound = document.createElement("audio");
  this.sound.src = src;
  this.sound.setAttribute("preload", "auto");
  this.sound.setAttribute("controls", "none");
  this.sound.style.display = "none";
  //document.appendChild(this.sound);
  this.play = function(){
    this.sound.play();
  }
  this.stop = function(){
    this.sound.pause();
  }
} 

