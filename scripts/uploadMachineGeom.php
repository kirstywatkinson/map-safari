<?php
/*
* Script to update verified building outline
*/


header('Content-Type: application/json');

//Get data from main mapping page
$post = file_get_contents('php://input');
$array = json_decode($post, true);


//Get feature id
$id =  $array["id"];

//Get feature geometry
$geom = $array["geom"];
$geom2 =  json_encode($geom);

//Get feature tag
$tag = $array["tag"];

// Get the user who uploaded it 
$user = $array["user"];

//Connect to database
require('./connection.php');

//Get user id
$user_id = pg_fetch_row(pg_query("SELECT user_id FROM users WHERE username = '$user'"))[0];

//Update feature in the database with the new geometry, the tag and its status
$query = "UPDATE buildings SET status = 4, building = '$tag', user_id = $userID, geom = ST_SetSRID(ST_GeomFromGeoJSON('$geom2'), 4326) WHERE gid = $id";
$result = pg_query($query);

//Get user points
$countQuery = pg_query("SELECT points FROM users WHERE username = '$user'");

//Add one onto the point total
$newCount = pg_fetch_row($countQuery)[0] + 1;
$updateCount = pg_query("UPDATE users SET points = '$newCount' WHERE username = '$user'");

//Update competition points - collective scores

$countQueryh = pg_query("SELECT human FROM competition LIMIT 1");
$newCounth = pg_fetch_row($countQueryh)[0] + 1;
$updateCounth = pg_query("UPDATE competition  SET human = '$newCounth'");
$countQueryC = pg_query("SELECT computer FROM competition LIMIT 1");
$newCountC = pg_fetch_row($countQueryC)[0] - 1;
$updateCountC = pg_query("UPDATE competition SET computer = '$newCountC'");

//Update human vs computer individual scores
$countQueryI = pg_query("SELECT computer FROM users WHERE username = '$user'");
$computer = pg_fetch_row($countQueryI)[0] - 1;
$countQueryI2 = pg_query("SELECT human FROM users WHERE username = '$user'");
$human = pg_fetch_row($countQueryI2)[0] + 1;
$updateCountI = pg_query("UPDATE users SET computer = '$computer', human = '$human' WHERE username = '$user'");

//Echo human score 
echo json_encode($human);

?>


