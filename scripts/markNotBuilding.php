<?php
/*
* Mark that outline is not a building 
*/

//get the outline id
$id = $_GET['id'];

//get username
$user = $_GET['user'];


//Connect to database
require('./connection.php');

//Get user id
$user_id = pg_fetch_row(pg_query("SELECT user_id FROM users WHERE username = '$user'"))[0];

//Set status as 3 to say not a building
$query = "UPDATE buildings SET status = 3, user_id = '$user_id' WHERE gid = $id";
$result = pg_query($query);

//Get user point total
$countQuery = pg_query("SELECT points FROM users WHERE username = '$user'");

//Update how many outlines the user has rejected and their points total
$newCount = pg_fetch_row($countQuery)[0] + 1;
$notBuilding = pg_fetch_row(pg_query("SELECT rejected FROM users WHERE username = '$user'"))[0];
$newVal = $notBuilding + 1;
$updateCount = pg_query("UPDATE users SET points = '$newCount', rejected = '$newVal'  WHERE username = '$user'"); 

//Update competition scores - collective

$countQueryh = pg_query("SELECT human FROM competition LIMIT 1");
$newCounth = pg_fetch_row($countQueryh)[0] + 1;
$updateCounth = pg_query("UPDATE competition SET human = '$newCounth'");
$countQueryC = pg_query("SELECT computer FROM competition LIMIT 1");
$newCountC = pg_fetch_row($countQueryC)[0] - 1;
$updateCountC = pg_query("UPDATE competition SET computer = '$newCountC'");

//Update competition scores - individual

//Update human vs computer individual scores
$countQueryI = pg_query("SELECT computer FROM users WHERE username = '$user'");
$computer = pg_fetch_row($countQueryI)[0] - 1;
$countQueryI2 = pg_query("SELECT human FROM users WHERE username = '$user'");
$human = pg_fetch_row($countQueryI2)[0] + 1;
$updateCountI = pg_query("UPDATE users SET computer = '$computer', human = '$human' WHERE username = '$user'");

echo json_encode($human);

?>

