<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="preconnect" href="https://fonts.gstatic.com">
<link href="https://fonts.googleapis.com/css2?family=Cabin:wght@400;500&display=swap" rel="stylesheet"> 
<link rel="stylesheet" href="https://unpkg.com/leaflet@1.7.1/dist/leaflet.css" />
<!-- Load JavaScript for Leaflet -->
<script src="https://unpkg.com/leaflet@1.7.1/dist/leaflet.js"></script>

<!-- Load Javascript for Bing tiles-->
<script src="./scripts/Bing.js"></script>
<script src="./scripts/Bing.addon.applyMaxNativeZoom.js"></script>
<!--load osm-auth -->
<script src="./scripts/osm-auth-main/osmauth.js"></script>
                <!-- load js require -->
                <script src="./node_modules/npm-require/src/npm-require.js"></script>
<!-- Polyfill-->
                <script src="https://cdn.polyfill.io/v2/polyfill.min.js?features=Promise"></script>

<style>
body, html{

	margin: 0;
	width: 100%;
	height: 100%;
}

* {

 box-sizing: border-box;
}

/* Create three equal columns that floats next to each other */
.column {
  float: left;
  width: 37.5%;
  height: 55%;
  background-color: #ffffff;

}


.columnside {

  float: right;
  width: 25%;
  border-left: 10px solid #ffffff;
  height: 100%;
  z-index: 0;
}


.column2 {

float: left;
  width: 100%;
  height: 100%;
	background-image: linear-gradient(rgba(247, 249, 251, 0.8), rgba(247, 249, 251, 0.8)), url(./Images/tile_tsp.png);
  background-position: center;
  background-size: 500px 500px;

}

.column3 {

float: left;
  width: 100%;
  height: 100%;
  border-top: 10px solid #ffffff;
  background-color: #ffffff;
}

.columnside2 {

  float: left;
  width: 25%;
  background-color: #8db48e;
  border-left: 10px solid #ffffff;
  height: 100%;

}


.row{

	height: 100%;
}
header{
	background-color: #ffffff;
	height: 45%;
	width: 75%;
}

footer{

	height: 20%;
	width: 75%;
}
/* Clear floats after the columns */
.row:after {
  content: "";
  display: table;
  clear: both;
}


h2{
  font-family: 'Cabin', sans-serif;
  text-align: center;
  /*color: #93B292;*/
  color:black;
  font-size: 40px;

  /*text-shadow: -1px -1px 0 #000000, 1px -1px 0 #000000, -1px 1px 0 #000000, 1px 1px 0 #000000;*/
}


h3 {

	text-align: center; 
	color: #ffffff;
	font-family: 'Cabin', sans-serif;
	font-size: 40px;
}

p {


}


p2 {

	font-family: 'Cabin', sans-serif;
	color:  #ffffff;
	line-height: 2;
	z-index: 5000;
	 position: relative;
	font-size: 25px;
	text-shadow: 2px 2px 5px black, 2px 2px 5px black;
}

.p3 {
        font-family: 'Cabin', sans-serif;
	font-weight: normal;
	color: #ffffff;
	line-height: 2;
	z-index: 5000;
	position: relative;
	font-size: 25px;
	text-shadow: 2px 2px 5px black, 2px 2px 5px black;
}

table, th {
			margin-left: 25%;
                        border: 1px solid black;
                        font-family: 'Cabin', sans-serif;
                        font-weight: bold;
                        font-size: 20px;
                        color: black;
                        text-align: center;
			padding: 10px;
			border-collapse:collapse;
                }

                td {
                        font-weight: normal;
                        border: 1px solid black;
			padding: 10px;
                }

tr {

	line-height: 20px;
}

ul {
              list-style-type: none;
              margin: 0;
              padding: 0;
              overflow: hidden;
	      display:inline-block;


                }

li {
                    float: right;
                    text-align: justify;
                    text-justify: inter-word;
                    display:inline-block;
                }

                li a {
                  display: block;
                  color: black;
                  text-align: center;
                  padding: 5px 7px;
                  text-decoration: none;
                  font-family: 'Noto Sans TC', sans-serif;
                    font-weight: 700;
                }

img {
                        height: 20px;
                        width: 20px;
			z-index: 6000;
                }

.scores {

		color:#ffffff;
		margin-right:2%;
		font-family: 'Cabin', sans-serif; 
		font-size:70px;
		text-align: center;
		text-shadow: -1px -1px 0 #999999, 1px -1px 0 #999999, -1px 1px 0 #999999, 1px 1px 0 #999999;
	}

.img2 {

	height:140px;
	width: 140px;
	

	}
@viewport {
                        width: device-width ;
                        zoom: 1.0 ;
                }


                @media screen and (max-width: 1500px) and (min-width: 1100px) {
			h2{
				font-size: 25px;
			}

			 h3{
                                font-size: 25px;
                        }

			.scores {
				font-size: 45px;

			}

			p2 {
				font-size: 20px;

			}

			table, th {
				font-size: 18px;
			}


		}
</style>
</head>
<body>

<div class="row">
<header>
<div class="column2" style="">
<div style="text-align:right"><ul>
                <li><a href="./home.php">HOME</a></li>
                <li><a href="./about.php" target="_blank">ABOUT</a></li>
                <li><a href="https://www.instagram.com/community_mapping_ug/" target="_blank"><img src="./images/twitter.png" alt="Instagram"></a></li>
                <li><a href="https://twitter.com/klwatkinson" target="_blank"><img src="./images/instaw.png" alt="Twitter"></a></li>
</div>

<div style="margin-top: 2%;text-align:center">
<img class="img2" src="./Images/human.png"></img> <span class="scores" style="margin-right:8%;margin-left:8%"> vs </span> <img class="img2" src="./Images/machine.png"></img><br>
<span id="human-score" class="scores"> </span> <span style="margin-right: 10%;margin-left:10%; color: transparent">vs</span> <span class="scores" id="machine-score"> </span> 
<br><p2 style="color: black; text-shadow:none">Together we have mapped <b><span id='area'></span> km<sup>2</sup></b> of the Acholi region and we are currently <b><span id="compResult"></span></b> the machine. Great work team, keep going!</p2>
<br></div>
</div>
  </header>

<!-- color: #93B292; text-shadow: -1px -1px 0 #000000, 1px -1px 0 #000000, -1px 1px 0 #000000, 1px 1px 0 #000000 -->

<div id='columnside' class="columnside" style="top:0;position:absolute;right:0;">
<div style="text-align:center; padding-top: 25%"><img src="./Images/logo.png" style="height:150px;width:150px; z-index:5000;"></img></div><br>
<div style="text-align: center"><p2>USERNAME: <span class="p3" id="user-name"> Insert text here</span></p2><br>
    <p2>POINTS TOTAL: <span class="p3" id="total-points"> Insert text here</span></p2><br>
    <p2>LEVEL: <span class="p3" id="level"> Insert text here</span></p2><br>
    <p2>BUILDINGS MAPPED: <span class="p3" id="mapped"> Insert text here</span></p2><br>
    <p2>FEATURES REJECTED: <span class="p3" id="rejected"> Insert text here</span></p2><br>
    <p2>UNSURE: <span class="p3" id="notSure"> Insert text here</span></p2><br>
    <p2>TOTAL EDITS: <span class="p3" id="allActions"> Insert text here</span></p2>


  </div>
</div>

  <div class="column" style="">
    <h2 style="">OVERALL POINTS LEADERBOARD</h2>
	<div style="text-align: center">
	<table style="">
                        <tr>
                        <th> Position</th>
                        <th> Username</th>
                        <th> Points</h>
                        </tr>
                        <tr>
                        <td>1st</td>
                        <td><span id="user-first"></span></td>
                        <td><span id="points-first"></span></td>
                        </tr>
                        <tr>
                        <td>2nd</td>
                        <td><span id="user-second"></span></td>
                        <td><span id="points-second"></span></td>
                        </tr>
                        <tr>
                        <td>3rd</td>
                        <td><span id="user-third"></span></td>
                        <td><span id="points-third"></span></td>
                        </tr>
                        <tr>
                        <td>4th</td>
                        <td><span id="user-fourth"></span></td>
                        <td><span id="points-fourth"></span></td>
                        </tr>
                        <tr>
                        <td>5th</td>
                        <td><span id="user-fifth"></span></td>
                        <td><span id="points-fifth"></span></td>
                        </tr>
                        </table>

		</div>

  </div>
  <div class="column" style="">

	<h2 style="">HUMAN VS MACHINE LEADERBOARD</h2>
		<div style="text-align: center">
		<table style="">
                        <tr>
                        <th> Position</th>
                        <th> Username</th>
                        <th> Points</h>
                        </tr>
                        <tr>
                        <td>1st</td>
                        <td><span id="h-user-first"></span></td>
                        <td><span id="h-points-first"></span></td>
                        </tr>
                        <tr>
                        <td>2nd</td>
                        <td><span id="h-user-second"></span></td>
                        <td><span id="h-points-second"></span></td>
                        </tr>
                        <tr>
                        <td>3rd</td>
                        <td><span id="h-user-third"></span></td>
                        <td><span id="h-points-third"></span></td>
                        </tr>
                        <tr>
                        <td>4th</td>
                        <td><span id="h-user-fourth"></span></td>
                        <td><span id="h-points-fourth"></span></td>
                        </tr>
                        <tr>
                        <td>5th</td>
                        <td><span id="h-user-fifth"></span></td>
                        <td><span id="h-points-fifth"></span></td>
                        </tr>
                        </table></div>
	
  </div>
</div>

</body>

<?php

                        require('./scripts/connection.php');

                        $user = $_POST['user'];

			$pointsQuery = pg_fetch_row(pg_query("SELECT points FROM users WHERE username = '$user'"))[0];

			$level = pg_fetch_row(pg_query("SELECT level FROM users WHERE username = '$user'"))[0];

			$machineMapped = pg_num_rows(pg_query("SELECT * FROM google WHERE username = '$user' AND status = 4")); 
			$userMapped = pg_num_rows(pg_query("SELECT * FROM usercreated WHERE username = '$user'"));

			$mapped = intval($machineMapped) + intval($userMapped);

			$rejected = pg_num_rows(pg_query("SELECT * FROM google WHERE username = '$user' AND status = 3"));

			$notSure = pg_num_rows(pg_query("SELECT * FROM google WHERE username = '$user' AND status = 5"));


//			$leaderboardQuery = pg_query("SELECT username, points FROM users ORDER BY points LIMIT 10");
			$leaderboardQuery = pg_query("SELECT username, points FROM users ORDER BY points");
//                        $leaderboardQuery2 = pg_query("SELECT username, human FROM users ORDER BY human LIMIT 10");
                        $leaderboardQuery2 = pg_query("SELECT username, human FROM users ORDER BY human");


                        $allScores = [];

                        while ($row = pg_fetch_row($leaderboardQuery)) {

                                $individual = [];

                                array_push($individual, $row[0]);
                                array_push($individual, $row[1]);
                                array_push($allScores, $individual);


                        }

                        $allScoresH = [];

                        while ($row = pg_fetch_row($leaderboardQuery2)) {

                                $individualH = [];

                                array_push($individualH, $row[0]);
                                array_push($individualH, $row[1]);
                                array_push($allScoresH, $individualH);


                        }


                        $competitionQuery = pg_query("SELECT human, computer FROM competition");

                        while ($row = pg_fetch_row($competitionQuery)){

                                $humans = $row[0];

                                $computer = $row[1];


                        }

			if ($humans > $computer){
				$compResult = "beating";


			} else {

				$compResult = "losing to";
			}

                        $scores = json_encode($allScores);

                        $scoresH = json_encode($allScoresH);


                        $gridQuery = pg_query("SELECT status FROM grid");

			$area = 0;

			$rowTest = pg_fetch_row($gridQuery)[0];

                         while ($row = pg_fetch_row($gridQuery)){


				if($row[0]  === "2"){

					$area += 1;

				}


                        }


                        //echo id and bounding box into main script
                        echo "<script>\n";
			echo "let user = '$user'\n";
			echo "let userPoints = $pointsQuery\n";
			echo "let level = '$level'\n";
                        echo "let scores = $scores\n";
                        echo "let scoresH = $scoresH\n";
                        echo "let humans = $humans\n";
                        echo "let computer = $computer\n";
			echo "let area = $area\n";
			echo "let compRes = '$compResult'\n";
                        echo "let mapped = '$machineMapped'\n";
			echo "let rejected = '$rejected'\n";
			echo "let notSure = '$notSure'\n";
			echo"</script>";



                ?>
<script>

                var OsmRequest = require('osm-request');

                const osm = new OsmRequest({
                endpoint: 'https://api06.dev.openstreetmap.org',
                oauthConsumerKey: 'VnCASv0JrF4mqHQ3uWBCE6bf1dHFh14ltHoGBbmV',
                oauthSecret: '9fAkZCyRDkS8yAySv9MJYf78uFM8wcERG3oK1081',
                auto: true});


		if (!osm._auth.bringPopupWindowToFront()) {
                                osm._auth.authenticate(function() {
                                        //Update function
                                        showDetails();
                                });
                } else {

		showDetails();

		}



                document.getElementById('human-score').innerHTML = humans;

                document.getElementById('machine-score').innerHTML = computer;






		scoresOrdered = scores.sort(compare);

                hScoresOrdered = scoresH.sort(compare);


                highScore = 0;

                for (i = 0; i < scores.length; i ++) {

                        if (scores[i] > highScore){
                                highScore = scores[i];

                        }
                        console.log(scores[i]);

                }

		document.getElementById('area').innerHTML = area;
		document.getElementById('compResult').innerHTML = compRes;

               document.getElementById('user-first').innerHTML = scoresOrdered[0][0];
                document.getElementById('points-first').innerHTML = scoresOrdered[0][1];
                document.getElementById('user-second').innerHTML = scoresOrdered[1][0];
                document.getElementById('points-second').innerHTML = scoresOrdered[1][1];
                document.getElementById('user-third').innerHTML = scoresOrdered[2][0];
                document.getElementById('points-third').innerHTML = scoresOrdered[2][1];
		document.getElementById('user-fourth').innerHTML = scoresOrdered[3][0];
                document.getElementById('points-fourth').innerHTML = scoresOrdered[3][1];
		document.getElementById('user-fifth').innerHTML = scoresOrdered[4][0];
                document.getElementById('points-fifth').innerHTML = scoresOrdered[4][1];



                document.getElementById('h-user-first').innerHTML = hScoresOrdered[0][0];
                document.getElementById('h-points-first').innerHTML = hScoresOrdered[0][1];
                document.getElementById('h-user-second').innerHTML = hScoresOrdered[1][0];
                document.getElementById('h-points-second').innerHTML = hScoresOrdered[1][1];
                document.getElementById('h-user-third').innerHTML = hScoresOrdered[2][0];
                document.getElementById('h-points-third').innerHTML = hScoresOrdered[2][1];
		document.getElementById('h-user-fourth').innerHTML = hScoresOrdered[3][0];
                document.getElementById('h-points-fourth').innerHTML = hScoresOrdered[3][1];
		document.getElementById('h-user-fifth').innerHTML = hScoresOrdered[4][0];
                document.getElementById('h-points-fifth').innerHTML = hScoresOrdered[4][1];


		//Bounds of Gulu district
        min_x = 32.12395;
        max_x = 32.82805;
        min_y = 2.4515;
        max_y = 3.3066;

        //Generate random coordinates within Gulu district
        let lon = min_y + (Math.random() * (max_y - min_y));
        let lat = min_x + (Math.random() * (max_x - min_x));

        console.log(lon);

        //set up map
        //Create a variable containingthe map object and add the basemaps as layers
        map = L.map('columnside', {minZoom: 15,
                            maxZoom: 18,
                            zoomControl: false

                        });

        //Load bing and OpenStreetMap basemaps
        const bing = L.bingLayer('AgWUG-xtDgsFNlDhzIMjiuvn2sqdQ82E_ywrY3jp_mbrOiIgW9w7YJ39oJiKwteg', {
                               maxZoom: 18}).addTo(map);



        //Set map view
        map.setView(L.latLng(lon,lat), 18);


        // prevent map from zooming when double-clicking
        map.doubleClickZoom.disable();

        //disable dragging
        map.dragging.disable();

        map.scrollWheelZoom.disable();

                function compare(a,b){return b[1] - a[1]}



		//Shows user name in top right corner - indicates they are logged in
                function done(err, res) {
                        // Error display
                        if (err) {
                                document.getElementById('user-name').innerHTML = 'error! try clearing your browser cache';
                                document.getElementById('user-name').style.display = 'inline-block';
                                return;
                        }

                        //Get username from returned xml
                        var u = res.getElementsByTagName('user')[0];
                        var o = {
                                display_name: u.getAttribute('display_name')
                        };

                        //set inner html of the user element as the username
                        for (var k in o) {
                                console.log(o[k]);
                                document.getElementById('user-name').innerHTML = o[k];
                        }

                        //Show the username on the webpage
                        document.getElementById('user-name').style.display = 'inline-block';
			document.getElementById('total-points').innerHTML = userPoints;
              		document.getElementById('level').innerHTML = level;
			document.getElementById('mapped').innerHTML = mapped;
			document.getElementById('rejected').innerHTML = rejected;
			document.getElementById('notSure').innerHTML = notSure;
		

			let total = parseInt(mapped) + parseInt(rejected) + parseInt(notSure);

			document.getElementById('allActions').innerHTML = total;




		  }

		//Get user details using the osm-request library
                function showDetails() {
                        osm._auth.xhr({
                        method: 'GET',
                        path: '/api/0.6/user/details'
                        }, done);

                }

                </script>

</html>
