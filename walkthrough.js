function stepOne(){

        //Remove highlight around move button
        document.getElementById("up").style.boxShadow = 'none';
        document.getElementById("down").style.boxShadow = 'none';
        document.getElementById("left").style.boxShadow = 'none';
        document.getElementById("right").style.boxShadow = 'none';

	 //disable move buttons
            document.getElementById("up").disabled = true;
            document.getElementById("down").disabled = true;
            document.getElementById("right").disabled = true;
            document.getElementById("left").disabled = true;

	
	//Change height of pop up box
	//document.getElementById("walkthrough").style.height = '12%';
        

        //Update message in pop up box
	document.getElementById("walkthroughHTML").innerHTML = 'Great work! Now you know how to move you can explore the area and look for buildings to map <button style="background-color:#4CAF50; width: auto;" id="walkthroughMove">OK, time to explore!</button>';

}

function stepTwo(event){
            

            if(event.target.id === 'walkthroughMove'){
	
            //Event listener for when they click a move button
            document.getElementById("arrows").removeEventListener("click", stepOne);


		editing = 'yes';
    
            //Update text in the pop up 
			document.getElementById("walkthroughHTML").innerHTML = 'You will come across buildings in three ways. The first are buildings that are already mapped. They are pink and look like this.<br><button style="background-color:#4CAF50; width: auto;" id="walkthroughOSM">Got it!</button> '

		//Check for OSM buildings

		if (osmLayer){	
            //Loop through the osm layer and find last feature
			osmLayer.eachLayer(function(layer) {

					
					demoFeature = layer.feature;	

					
					


			});

			            //Get centroid coordinates of that feature
                        let centre = turf.centroid(demoFeature);

	            //Zoom map to that feature
                        map.setView([centre.geometry.coordinates[1],centre.geometry.coordinates[0]], 20);


		} else {

			//Get centre of bounds of buildings Layer map
                        let layerCentre = buildingsLayer.getBounds().getCenter();
                        let point = turf.point([layerCentre.lng, layerCentre.lat]);

                        //Create a fake hut and add it to the map
		        let fakeHut = turf.buffer(point, 2, {units: 'meters'});
			let demoHut = L.geoJson(fakeHut, {style: {
                                        color: '#f21b7d',

                                        weight: 1,

                                        fillColor: '#ff91c3',

                                        fillOpacity: 0.3

                                        }}).addTo(map);

			//Zoom to the building	
			map.setView(layerCentre, 20);

		}



	}
}

function stepThree(event){
    
    
                if(event.target.id === 'walkthroughOSM'){

		    //Event listener for when they click a move button
	            document.getElementById("arrows").removeEventListener("click", stepTwo);
                
                    //Update pop up text
                    document.getElementById("walkthroughHTML").innerHTML = 'The next type are feature that the computer thinks is a building. They have blue outlines and look like this. <br><button style="background-color:#4CAF50; width: auto;" id="walkthroughMachine1">Got it!</button>' 
                    
				    //Loop through buildingsLayer and find last feature to use as demo
                    buildingsLayer.eachLayer(function(layer){


					   demoFeature = layer.feature;

				    })

                //Get centroid coordinates of that feature
				centre = turf.centroid(demoFeature);


		                //map.panTo([centre.geometry.coordinates[1],centre.geometry.coordinates[0]]);
                
                //Zoom map to that feature
				map.setView([centre.geometry.coordinates[1],centre.geometry.coordinates[0]], 20);

                }


}

function stepFour(event){

            if(event.target.id === 'walkthroughMachine1'){
            
            //Remove event listener for when they click a move button
            document.removeEventListener("click", stepThree);
            

            //Update pop up text box
            document.getElementById("walkthroughHTML").innerHTML = 'When you get close enough six buttons will appear on the screen and white corner circles will appear around the feature that allow you to edit it. Your first job is decide if it is a building or not. You can use the <b>Hide outline</b> button to see what is underneath. You can then click the <b>Show outline</b> button to make it reappear. You can also toggle the visibility using the <b>H</b> button on your keyboard. Have a go!'

					//Initialise empty array for coordinates
		   coords = [];


					// Loop length of the data
                    for (let i = 0; i < demoFeature.geometry.coordinates[0].length; i++){

                                  			//push feature coordinates
                                            coords.push([demoFeature.geometry.coordinates[0][i][1],demoFeature.geometry.coordinates[0][i][0]])

                                        }

                    //Create feature as a polygon
                    activeBuilding = L.polygon(coords, {transform: true, draggable:true}).addTo(map);

                    //Loop through buildingsLayer and find the matching feature and remove so it can't be seen by the user
					buildingsLayer.eachLayer(function(layer){

						if(layer.feature.geometry.coordinates === demoFeature.geometry.coordinates){

							map.removeLayer(layer);

						}
                                	})

                    //Set style as visual notification for user they can edit
					activeBuilding.setStyle({
                                                                                color: '#00ffb3',

                                                                                weight: 1.5,

                                                                                fillColor: '#00ffb3',

                                                                                fillOpacity: 0

                                                        });

                    //Enable editing on feature
                    activeBuilding.transform.enable({boundsOptions:{color: 'black'}, rotateHandleOptions:{color:'black'}, uniformScaling: true, dragging:true});

                    //Show editing buttons
                    document.getElementById("uploadDemo").style.display = 'block';
                    document.getElementById("notSureDemo").style.display = 'block';
                    document.getElementById("notBuildingDemo").style.display = 'block';
                    document.getElementById("circularise").style.display = 'block';
                    document.getElementById("reset").style.display = 'block';
                    document.getElementById("hideOutline").style.display = 'block';
                        
                    //Add glow to reset button
				    document.getElementById("hideOutline").style.boxShadow = '0 0px 25px #00ffff';
	            }




}



function showOutlineDemo(event){

	if(event.target.id === 'showOutline' || event.key == 'h'){

		//Remove event listener for when they click a move button
                document.removeEventListener("click", stepFour);

		document.getElementById("hideOutline").style.boxShadow = 'none';
		document.getElementById("hideOutline").style.boxShadow = 'none';

		document.getElementById("walkthroughHTML").innerHTML = 'Nice! If you think it is a building you have the option to edit the feature before uploading. You resize and rotate the feature using the white circle markers. Have a go at this and then click the <b>Reset feature</b> button or <b>R</b> on your keyboard.'

		document.getElementById("reset").style.boxShadow = '0 0px 25px #00ffff';

	}


}



function stepFive(event){

        if(event.target.id === 'reset' || event.key == 'r'){


        
                        //Remove showOutlineDemo event listener and re-add game event listener
			document.removeEventListener("click", showOutlineDemo);
			document.getElementById("showOutline").addEventListener("click", showOutline);
        
			//Reset building geometry
			resetBuilding(event);

                        //Remove glow from reset button
                        document.getElementById("reset").style.boxShadow = 'none';
		
			//Update pop up box text
                        document.getElementById("walkthroughHTML").innerHTML = 'Great work! You can also drag the feature. Simply click on the feature and hold the click as you  drag it to your desired position. Release the click to place the feature. Have a go!'						

			//Add event listener for when feature is dragged
			activeBuilding.addEventListener("drag",stepSix);

                
        }

}

function stepSix(event){


                            //Remove event listener for when they click a move button
                            document.removeEventListener("click", stepFive);

			    //Remove glow from reset button
	                    document.getElementById("reset").addEventListener("click", resetBuilding);

                            //Update pop up text
                            document.getElementById("walkthroughHTML").innerHTML = 'Excellent! There is another tool to help you edit. The circularise button. When pressed this will turn the feature into a circle. This is useful when you are presented a square feature that should be a circle. Such as a round hut. Have a go and click the <b>Circularise</b> button or <b>C</b> on your keyboard.'  
							
                            //highlight circularise button
							document.getElementById("circularise").style.boxShadow = '0 0px 25px #00ffff';



}

function stepSeven(event){

         if(event.target.id === 'circularise' || event.key == 'c'){


         
            //Remove event listener for when they drag the building
            activeBuilding.removeEventListener("drag", stepSix);
         
            //Remove highlight from circularise button
	    document.getElementById("circularise").style.boxShadow = 'none';

            //Update pop up box text
	    document.getElementById("walkthroughHTML").innerHTML = 'Nice! You are really getting good at this! Now you know how to edit your feature, you need to learn how to approve or reject the computer generated buildings. <br> <button style="background-color:#4CAF50; width: auto;" id="walkthroughButtons">How do I do that?</button>'
         

         }


}

function stepEight(event){

	if(event.target.id === 'walkthroughButtons'){

		//remove event listener
		document.removeEventListener("click", stepSeven);

		//readd circularise event listener
		document.getElementById("circularise").addEventListener("click", circularise);

		resetBuilding(event);


		//Update pop up box text
		document.getElementById("walkthroughHTML").innerHTML = 'The first button to know is <b>Not sure</b>. You can click on this when you are not sure if the feature is a building or not or if the satellite image is unclear. When clicked your feature will return to a blue outline without editing. Have a go!'


		document.getElementById("notSureDemo").style.boxShadow = '0 0px 25px #00ffff';

		document.getElementById("notSureDemo").addEventListener("click", notSure);
	}


}

function notSure(event){

	//remove event listener
                document.removeEventListener("click", stepEight);
		document.getElementById("notSureDemo").style.boxShadow = 'none';
		document.getElementById("notSureDemo").removeEventListener("click", notSure);

		activeBuilding.transform.disable();

		//Set style as visual notification for user they can edit
                                        activeBuilding.setStyle({

								color: '#2bdfff',

                                        			weight: 1,

                                        			fillColor: '#2bdfff',

                                        			fillOpacity: 0

                                                        });


		document.getElementById("walkthroughHTML").innerHTML = 'Nice! Next we will learn about the <b>Upload building</b> button. For the purpose of the demo click the building to re-enable editing.'


		document.getElementById("uploadDemo").style.boxShadow = '0 0px 25px #00ffff';

		activeBuilding.addEventListener("click", renable1);
}

function renable1(event){

		renable();


		//Update pop up text
		document.getElementById("walkthroughHTML").innerHTML = 'The <b>Upload building</b> button is used when you are ready to upload your feature as a building. This can be with or without editing. When clicked your feature will be upload to OpenStreetMap and will change to the same colour as already mapped buildings. Press the button to see what happens!'

		//Add event listener for upload button
		document.getElementById("uploadDemo").addEventListener("click", demoUpload);

		//Remove event listener from feature
		activeBuilding.removeEventListener("click", renable1);
}

function demoUpload(event){

		//Remove upload event listener
		document.getElementById("uploadDemo").removeEventListener("click", demoUpload);

		//Remove highlight
		document.getElementById("uploadDemo").style.boxShadow = 'none';

		//Disable transform
		activeBuilding.transform.disable();

		//Change style to match uploaded buildings
		 activeBuilding.setStyle({

						color: '#f21b7d',

	                                        weight: 1,

	                                        fillColor: '#ff91c3',

	                                        fillOpacity: 0.3
					
                                                        });

		//Play sound so user knows it's uploaded/they've gained points
                pointSound.play();

		//Update pop up text
		document.getElementById("walkthroughHTML").innerHTML = 'Great job! Finally we will learn about the <b>Not a building</b> button. For the purpose of the demo click the building to re-enable editing.';

		//Highlight not a building button
		document.getElementById("notBuildingDemo").style.boxShadow = '0 0px 25px #00ffff';

		//Add event listener to activebuilding to renable editing
                activeBuilding.addEventListener("click", renable2);

}

function renable2(event){

		//renable
		renable();


			
		//Update pop up text
		document.getElementById("walkthroughHTML").innerHTML = 'The <b>Not a building</b> button is used when you think the feature is not a building. When clicked the feature will be removed from the map. Try pressing the button.'

		//Remove upload event listener
                document.getElementById("notBuildingDemo").addEventListener("click", removeFeature);
		
}

function removeFeature (event){

		//Remove upload event listener
                document.getElementById("notBuildingDemo").removeEventListener("click", removeFeature);


		//Highlight not a building button
                document.getElementById("notBuildingDemo").style.boxShadow = 'none';


		//Set style as visual notification for user they can edit
                                        activeBuilding.setStyle({
                                                                                color: '#00ffb3',

                                                                                weight: 0,

                                                                                fillColor: '#00ffb3',

                                                                                fillOpacity: 0

					});	

		activeBuilding.transform.disable();


		//Update pop up text
                document.getElementById("walkthroughHTML").innerHTML = 'Excellent! Now we know how to work with computer generated buildings, it is time to move on to the final building you will come across: Buildings not found by either the computer or other mappers. Ready? <br><button style="background-color:#4CAF50; height: auto;" id="addOwn">Born ready!</button>'
	}





function stepNine(event){


		if(event.target.id === 'addOwn'){


			document.getElementById("addDemo").style.display = 'block';

			//Hide machine editing buttons			
			 document.getElementById("notSureDemo").style.display = 'none';
			document.getElementById("notBuildingDemo").style.display = 'none';
			document.getElementById("uploadDemo").style.display = 'none';
			document.getElementById("reset").style.display = 'none';
			document.getElementById("circularise").style.display = 'none';
			document.getElementById("hideOutline").style.display = 'none';
			
			//Set style as visual notification for user they can edit
                                        activeBuilding.setStyle({

                                                                color: '#2bdfff',

                                                                weight: 1,

                                                                fillColor: '#2bdfff',

                                                                fillOpacity: 0

                                                        });
			


			//Update pop up text
	                document.getElementById("walkthroughHTML").innerHTML = 'When you want to add a building that has not been found by the computer or other mappers. Click the <b>Add own building</b> button on the right of your screen. Have a go!'

			//Add event listener for add own button
//			document.getElementById("addDemo").addEventListener("click", addBuildingDemo);

			document.getElementById("addDemo").style.boxShadow = '0 0px 25px #00ffff';

		}



}


function addBuildingDemo(event){


		//Show upload button
		document.getElementById("uploadCreatedDemo").style.display='block';
	
		document.removeEventListener("click", stepNine);

		//Hide add own button
		document.getElementById("addDemo").style.display='none';

		//Update pop up text
		document.getElementById("walkthroughHTML").innerHTML = 'You will see a menu has appeared in the bottom right of your screen. This allow you to draw polygons, circles and rectangles/squares. To learn how to draw these shapes click on the icons below this text.<br><button id="squareDemo"style="margin-left:1%"><span class="iconify" data-icon="bi:square-fill" data-inline="false"></span></button><button id="circleDemo" style="margin-left:1%"><span class="iconify" data-icon="akar-icons:circle-fill" data-inline="false"></span></button><button id="polygonDemo"style="margin-left:1%"> <span class="iconify" data-icon="bi:pentagon-fill" data-inline="false"></span></button><button style="background-color:#4CAF50; width: auto; margin-left:1%; position:absolute;right: 5%" id="editVideosFinished1">Got it!</button>';

		//Create feature group where drawn items will be added to and add to map
                demoItems = new L.FeatureGroup();
                map.addLayer(demoItems);

		//Add new draw control
                demoControl = new L.Control.Draw({

                                        position: 'bottomright',
                                        edit: {marker: false,
                                                line: false,
                                                featureGroup: demoItems,
                                                poly: {
                                                        allowIntersection: false
                                                        }
                                                },
                                        draw: {

                                                polygon: {
                                                        allowIntersection: false,
                                                        showArea: false
                                                        },
                                                circle:  {showRadius: false},
                                                rectangle: {showArea: false},
                                                polyline: false,
                                                marker: false,
                                                circlemarker: false
                                                }
                                });

			demoControl.setDrawingOptions({
                                         rectangle: {
                                                shapeOptions: {
                                                        color: '#FFFFFF'
                                                }
                                        },
                                        circle: {
                                                shapeOptions: {
                                                        color: '#FFFFFF'
                                                }
                                        },
                                        polygon: {
                                                shapeOptions: {
                                                        color: '#FFFFFF'
                                                }
                                        }

                                });

                        map.addControl(demoControl);

			//Add event listener for add own button
		        document.getElementById("addDemo").removeEventListener("click", addBuildingDemo);

}

function stepTen(event){

	
	if(event.target.id === 'squareDemo'){

		document.getElementById("demoVideo").innerHTML = '<img src="./Images/square.gif" height="243" width="320"></img>';

		document.getElementById("editingDemo").style.display = 'block';

//		videoCount += 1;


	} else if(event.target.id === 'circleDemo'){
		document.getElementById("demoVideo").innerHTML = '<img src="./Images/circle.gif" height="243" width="320"></img>';

		document.getElementById("editingDemo").style.display = 'block';

//		videoCount += 1;

	} else if(event.target.id === 'polygonDemo'){

		document.getElementById("demoVideo").innerHTML = '<img src="./Images/polygon.gif" height="243" width="320"></img>';
		document.getElementById("editingDemo").style.display = 'block';

//		videoCount += 1;

		console.log(videoCount);
	
	}



}

function stepEleven(event){

		if(event.target.id === 'editVideosFinished1'){

			document.removeEventListener("click", stepTen);

			document.getElementById("editingDemo").style.display = 'none';

			document.getElementById("walkthroughHTML").innerHTML = 'You will have also noticed two other buttons. The <span class="iconify" data-icon="akar-icons:edit" data-inline="false"></span> button allows you to edit features you have drawn. The <span class="iconify" data-icon="foundation:trash" data-inline="false"></span> button allows you to delete features you have drawn. Click the buttons below to watch this in action. <br><button id="editDemo" style="margin-left:1%"><span class="iconify" data-icon="akar-icons:edit" data-inline="true"></span></button><button id="deleteDemo" style="margin-left:1%"><span class="iconify" data-icon="foundation:trash" data-inline="true"></span></button><button style="background-color:#4CAF50; width: auto; position:absolute;right: 5%" id="editVideosFinished2">Got it!</button>'

			//videoCount = 0;

		}

}

function stepTwelve(event){


	
	if(event.target.id === 'editDemo'){

		document.removeEventListener("click", stepEleven);

                document.getElementById("demoVideo").innerHTML = '<img src="./Images/edit.gif" height="243" width="320"></img>';

                document.getElementById("editingDemo").style.display = 'block';

//                videoCount += 1;


        } else if(event.target.id === 'deleteDemo'){
                
		document.removeEventListener("click", stepEleven);

		document.getElementById("demoVideo").innerHTML = '<img src="./Images/delete.gif" height="243" width="320"></img>';

                document.getElementById("editingDemo").style.display = 'block';
		

//                videoCount += 1;

        }



}


function stepThirteen(event){

	if(event.target.id === 'editVideosFinished2'){

		document.removeEventListener("click", stepTwelve);

		document.getElementById("editingDemo").style.display = 'none';


		document.getElementById("walkthroughHTML").innerHTML = 'Now you know how to draw your own buildings, have a go yourself as just demonstrated!';


		//Detect drawing events
                        map.on(L.Draw.Event.CREATED, function (e) {
                                        layer = e.layer;

                                        let type = e.layerType

                                        demoItems.addLayer(layer);



					document.getElementById("walkthroughHTML").innerHTML = 'Nice work! Once you are happy with your drawn buildings you can upload them using the <b>Upload building</b> button below the arrows.'
					
					document.getElementById("uploadCreatedDemo").style.boxShadow = '0 0px 25px #00ffff';

                        });

                        map.on('draw:edited', function (e) {
                                         var layers = e.layers;
                                        layers.eachLayer(function (layer) {

                                         });
                                });

	}

}

function stepFourteen(event){

		if(event.target.id === 'uploadCreatedDemo'){



			map.removeControl(demoControl);

			//Set the style of drawnBuildings so it matches osm layer
                                                        demoItems.setStyle({
                                                                                color: '#f21b7d',

                                                                                weight: 1,

                                                                                fillColor: '#ff91c3',

                                                                                fillOpacity: 0.3

                                                        });
		

			//Play sound so user knows it's uploaded/they've gained points
	                pointSound.play();
	
			document.getElementById("uploadCreatedDemo").style.display = 'none';

			document.getElementById("add").style.display = 'block';

			document.getElementById("walkthroughHTML").innerHTML = 'Amazing! That is everything! The last thing to tell you is about the leaves you may have spotted. These replace energy I lose whilst exploring the area with you. Make sure not to run out or you risk giving bonus points to the computer! If you need a reminder of anything at any time, click on the yellow <b>Help!</b> button in the top right corner. Now you know everything, are you ready to map? <br><button style="background-color:#4CAF50; width: auto;" id="tutorialFinished">Time to explore!</button>'

		}


}

function finalStep(event){

		if(event.target.id === 'tutorialFinished'){
		map.removeLayer(demoItems);

		document.getElementById("walkthrough").style.display = 'none';
		
		let Lcoords = avatar.getLatLng();

		map.setView(Lcoords, 20);

		editing = 'no';

		//disable move buttons
		document.getElementById("up").disabled = false;
		document.getElementById("down").disabled = false;
		document.getElementById("right").disabled = false;
	        document.getElementById("left").disabled = false;



			//Remove the grid that hides the other squares from view
    map.addLayer(smallerGrid);
		}
	}

function renable(){

		//Enable editing on feature
                activeBuilding.transform.enable({boundsOptions:{color: 'black'}, rotateHandleOptions:{color:'black'}, uniformScaling: true, dragging:true});

		//Set style as visual notification for user they can edit
                                        activeBuilding.setStyle({
                                                                                color: '#00ffb3',

                                                                                weight: 1.5,

                                                                                fillColor: '#00ffb3',

                                                                                fillOpacity: 0

                                                        });

		
}
