<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">  		
		<link rel="shortcut icon" href="#">
                <!--load osm-auth -->
                <script src="./scripts/osm-auth-main/osmauth.js"></script>
                <!-- load js require -->
                <script src="./node_modules/npm-require/src/npm-require.js"></script>
                <!-- Load CSS for Leaflet -->
                <link rel="stylesheet" href="https://unpkg.com/leaflet@1.7.1/dist/leaflet.css" />
                <!-- Load JavaScript for Leaflet -->
                <script src="https://unpkg.com/leaflet@1.7.1/dist/leaflet.js"></script>
            
                <!-- Load CSS for Context Menu-->
                <link rel="stylesheet" href="./scripts/leaflet.contextmenu.css"/>
                <!-- Load Javascript for Turf -->
                <script src="https://cdn.jsdelivr.net/npm/@turf/turf@5/turf.min.js"></script>
                <!-- Load Javascript for Bing tiles-->
                <script src="./scripts/Bing.js"></script>
                <script src="./scripts/Bing.addon.applyMaxNativeZoom.js"></script>
                <!-- Load fonts -->
                <link href="https://fonts.googleapis.com/css2?family=Heebo:wght@400;600&display=swap" rel="stylesheet">
                <!-- Polyfill-->
                <script src="https://cdn.polyfill.io/v2/polyfill.min.js?features=Promise"></script>
        
		<style>
            body, html{
                        width: 100%;
                        height: 100%;
                        margin: 0;
                }

            /* The map */
            #map {
                        width: 100%;
                        height: 100%;
                        z-index: 0;
                }
        
            
        ul {
              list-style-type: none;
              margin: 0;
              padding: 0;
              overflow: hidden;
	      z-index: 500;
            }

            li {
              float: left;
		z-index: 500;
            }

            li a {
              display: block;
              color: white;
              text-align: center;
              padding: 18px 16px 0px 18px;
              text-decoration: none;
              font-family: 'Heebo', sans-serif;
                font-weight: 400;
            }
                        
			
		/* style the container */
		/*#container {
			width: 100%;
			height: 100%;

		}*/
		
		/* style the header text */
		#header-text {
			font-family: 'Heebo', sans-serif;
            		font-weight: 700;
			text-align: center;
			font-weight: bold;
			font-size: 72px;
			position: relative;
			color: white;
			<!-- left: 100px; -->
			<!-- top: 17px; -->
		}
		
				
		/* style the header text */
		#paragraph-text {
			top: 15%;
			color: white;
			font-family: 'Heebo', sans-serif;
			position: relative;
			padding: 0px 30px 10px 30px;
			text-align: center;
			z-index: 500;
			width:60%;
			left: 20%;
		}
		
		.wrapper{
			width: 100%;
			text-align: right;
						
		}
		
		
		p {
			font-size: 24px;


		}

		p2 {
			font-size: 30px;

		}

		#paragraph-a {
			color: white;

		}
		#paragraph-a:visited {

			color: white;

		}

		/* style the container */
		#button-container {
			text-align: center;
			width: 60%;
			height: 20%;
			position: relative;
			left:20%;
			top: 15%;
		}
	
		/* style the container */
                #login-container {
                        text-align: center;
                        width: 100%;
                }
		
		/* Button styles */
		.button {
			position: relative;
			border: none;
			width: 6%;
			height: 100%;
			display: inline;
			cursor: pointer;
			overflow-wrap: break-word;
			z-index: 500;
			background: none;
		}

		.buttone {
			width: 16%;
			height: 80%;

		}

		.buttonl {
			width: 13%;
			height: 70%;
		}

		.buttonh {
			width: 12%;
			height: 70%;
		}
		
		.buttonr {
                        width: 14%;
                        height: 70%;
                }

		
		#header-container{
		
			width: 60%;
			left: 20%;
			text-align: center;
			position: relative;
			z-index: 500;
			
		}

		#ul-container{
			width: 100%;
                        text-align: right;
                        position: relative;
                        z-index: 500;



		}

		
		.button2{
			margin-top: 15px;
			padding-right: 5px;
                        padding-top: 5px;
                        padding-bottom: 5px;
                        padding-left: 5px;
                        background-color: white;
                        border: black 2px solid;
                        cursor: pointer;
                        font-family: 'Heebo', sans-serif;
                        font-weight: bold;
                        font-size: 16px;
                        width: auto;
                        color: black;
			z-index: 500;
                }

        
		.button2:hover{
			border: black 3px solid;

		}


		table, th {
 		 	border: 1px solid black;
			font-family: 'Heebo', sans-serif;
                        font-weight: bold;
                        font-size: 16px;	
			background-color: white;
			color: black;
			text-align: center;
		}

		td {
			font-weight: normal;
			border: 1px solid black;
		}


		.rhino {
                        height:100%;
                        width:100%;

                }

                .giraffe {
                        height:100%;
                        width:100%;
                }

                .elephant {
                        height:100%;
                        width:100%;
                }

                .hippo {
                        height:100%;
                        width:100%;

                }

                .lion {
                        height:100%;
                        width:100%;
                }


		@viewport {
                        width: device-width ;
                        zoom: 1.0 ;
                }

		@media screen and (max-width: 1500px) and (min-width: 1100px){

		p {

			font-size: 20px;
		}

		.button {
                        width: 7%;
                        height: 100%;
                }

                .buttone {
                        width: 18%;
                        height: 80%;

                }

                .buttonh {
                        width: 15%;
                        height: 75%;
                }

		  .buttonl {
                        width: 15%;
                        height: 75%;
                }

                .buttonr {
                        width: 15%;
                        height: 75%;
                }


		
		#button-container {
			width: 70%;

			left: 15%;
		}

		#header-container{
                        width: 70%;
                        left: 15%;

                }

                 #paragraph-text{
                        width: 70%;
                        left: 15%;

                }


		#header-text {
			font-size: 65px;

		}

		}

		@media screen and (max-width: 1099px) and (min-width: 600px){
		
		p {

                        font-size: 20px;
                }

		.button2{

			font-size: 13px;
		}

                #button-container {
                        width: 90%;

			left: 5%;
                }

		#header-container{
			width: 80%;
			left: 10%;

		}
			
		 #paragraph-text{
                        width: 80%;
                        left: 10%;

                }



                #header-text {
                        font-size: 50px;

                }

		.button {
                        width: 9%;
                        height: 100%;
                }

                .buttone {
                        width: 24%;
                        height: 80%;

                }

                .buttonh {
                        width: 19%;
                        height: 75%;
                }

		.buttonl {
                        width: 19%;
                        height: 75%;
                }

		.buttonr {
                        width: 19%;
                        height: 75%;
                }

		}


	
		</style>
	</head>
	
	<body>
		
            <div id="map">
			
            <!-- header -->
			
			
			<div id='ul-container'>
		<ul>
		<li style="margin-left: 3px; margin-right: 5px; float:right"><button class="button2" onclick="statsPage()">GAME STATS</button></li>
               	<li style="float:right; margin-left: 3px;"><a class="button2" href="./about.php" target="_blank">ABOUT</a></li>
		<li style="float:right"><button id="logout" class="button2" style="display: none">LOG OUT</button></li>
		</ul>

		</div>
			
<br>
			
			<div id='header-container'>
			<div id='header-text' style="margin-top:10%">
			<h style=""> MAP SAFARI </h>
            		<!--<p style="">mapping areas of the world that need our help, one game at a time</p>-->
			<p>Apowyo bino! (or welcome for non-Acholi speakers) </p>
			<p style="font-weight: normal">Are you ready to take on the machine in a building mapping battle, all whilst exploring the beautiful region of <a id='paragraph-a' href="https://en.wikipedia.org/wiki/Acholi_people#/media/File:Acholiland,_Uganda.png" target="_blank">Acholi</a> in Northern Uganda? Pick your favourite Ugandan animal and take a trip around the region, finding buildings to map in order to provide the region with an up-to-date detailed map and discover more about the country of Uganda.</p>
			<p style="font-weight: normal">To start playing log in to your <a id= "paragraph-a" href='https://www.openstreetmap.org' target="_blank">OpenStreetMap</a> account:</p>
			</div>
			<div style="width:54%; float: right;">
			<button class="button2" id="authenticate">LOG IN</button><button id="logout" style="display: none">LOG OUT</button>
			</div>
			</div>
			<br>
			<div id='paragraph-text'>
			<div id='username'>
			<p2> Hi there <span id='name' style="font-weight:bold;"></span>!</p2>
			<p> The aim of the game is to explore areas of the Acholi region and find buildings to map. Mapping buildings gains you points both as individual and a collective team of mappers, building up points in order to beat the computer and create up-to-date maps of the region. Be sure to keep an eye on your energy levels as you need it to stay ahead of the computer, though there may be a few tasty treats to keep you mapping!</p> 
			<p>To begin choose your character:</p>
			</div>
			</div>
			<div id='button-container'>
			<button class="button buttonr" id="rhino" onclick="avatar(this.id)"><img src='./Images/rhino.png' class="rhino"></button>
			<button class="button" id="giraffe" onclick="avatar(this.id)"><img src='./giraffe.png' class="giraffe"></img></button>
			<button class="button buttone" id="elephant" onclick="avatar(this.id)"><img src='./elephant.png' class="elephant"></button>
			<button class="button buttonh" id="hippo" onclick="avatar(this.id)"><img src='./hippo.png' class="hippo"></button>
			<button class="button buttonl" id="lion" onclick="avatar(this.id)"><img src='./Images/lion.png' class="lion"></button>
			</div>
			</div>
		</div>

<?php

			require('./scripts/connection.php');

			$leaderboardQuery = pg_query("SELECT username, points FROM users ORDER BY points LIMIT 5");


			$allScores = [];

			while ($row = pg_fetch_row($leaderboardQuery)) {

				$individual = [];

				array_push($individual, $row[0]);
				array_push($individual, $row[1]);
				array_push($allScores, $individual);


			}

			

			$scores = json_encode($allScores);

			//echo id and bounding box into main script
			echo "<script>\n";
			echo "let scores = $scores\n";
			echo"</script>";


			

		?>
<script>

let userName;

		var OsmRequest = require('osm-request');

		const osm = new OsmRequest({
                endpoint: 'https://api06.dev.openstreetmap.org',
                oauthConsumerKey: 'VnCASv0JrF4mqHQ3uWBCE6bf1dHFh14ltHoGBbmV',
                oauthSecret: '9fAkZCyRDkS8yAySv9MJYf78uFM8wcERG3oK1081',
                auto: true});
                
        //Bounds of Gulu district
        min_x = 32.12395;
        max_x = 32.82805;
        min_y = 2.4515;
        max_y = 3.3066;

        //Generate random coordinates within Gulu district
        let lon = min_y + (Math.random() * (max_y - min_y));
        let lat = min_x + (Math.random() * (max_x - min_x));
        
        console.log(lon);

        //set up map
        //Create a variable containingthe map object and add the basemaps as layers
        map = L.map('map', {minZoom: 15,
                            maxZoom: 18,
                            zoomControl: false
                        });
            
        //Load bing and OpenStreetMap basemaps
        const bing = L.bingLayer('AgWUG-xtDgsFNlDhzIMjiuvn2sqdQ82E_ywrY3jp_mbrOiIgW9w7YJ39oJiKwteg', {
                               maxZoom: 18}).addTo(map);
                                        
        //Set map view
        map.setView(L.latLng(lon,lat), 18);
                 
        // prevent map from zooming when double-clicking 
        map.doubleClickZoom.disable();

        //disable dragging
        map.dragging.disable();      

	map.scrollWheelZoom.disable();  
            

        document.getElementById('authenticate').onclick = function() {
           		if (!osm._auth.bringPopupWindowToFront()) {
                		osm._auth.authenticate(function() {					
					//Update function
                    			update();
                		});
            		}
        	};

		document.getElementById('logout').onclick = function() {
           		 osm._auth.logout();
            		update();
        	};

		function update() {
			if (osm._auth.authenticated()) {

				//Update value of name so that it contains the username
				showDetails();			

				document.getElementById('logout').style.display = 'block';
                document.getElementById('authenticate').style.display = 'none';
				document.getElementById('header-text').style.display = 'none';
				document.getElementById('username').style.display = 'block';
				document.getElementById('paragraph-text').style.display = 'block';
				document.getElementById('button-container').style.display = 'block';
            		} else {
           		document.getElementById('logout').style.display = 'none';
                document.getElementById('authenticate').style.display = 'block';
				document.getElementById('username').style.display = 'none';
				document.getElementById('header-text').style.display = 'block';
				document.getElementById('button-container').style.display = 'none';
		 	}
        	}

		//Shows user name in top right corner - indicates they are logged in    
        function done(err, res) {
                	// Error display
            		if (err) {
	                	document.getElementById('name').innerHTML = 'error! try clearing your browser cache';
        	        	document.getElementById('name').style.display = 'inline-block';
                		return;
            		}
                                
            		//Get username from returned xml
            		var u = res.getElementsByTagName('user')[0];
            		var o = {
                		display_name: u.getAttribute('display_name')
            		};

            		//set inner html of the user element as the username
            		for (var k in o) {
					console.log();
					userName = o[k];
                		document.getElementById('name').innerHTML = o[k];
            		}

            		//Show the username on the webpage
            		document.getElementById('name').style.display = 'inline-block';
        	}

	        //Get user details using the osm-request library
        	function showDetails() {
            		osm._auth.xhr({
                	method: 'GET',
                	path: '/api/0.6/user/details'
            		}, done);
            
        	}

		update();

		function avatar(animal) {


                                let f = document.createElement('form');
                                f.action = './mapping.php';
                                f.method = 'POST';

                                let i = document.createElement('input');
                                i.type ='hidden';
                                i.name ='avatarChoice';
                                i.value = animal;
                                f.appendChild(i);
				

                                document.body.appendChild(f);
                                f.submit();


                        }

		function statsPage(){
			let f = document.createElement('form');
                                f.action = './stats2.php';
                                f.method = 'POST';

                                let i = document.createElement('input');
                                i.type ='hidden';
                                i.name ='user';
                                i.value = userName;
                                f.appendChild(i);


                                document.body.appendChild(f);
                                f.submit();


		}
        
		</script>
	</body>

</html>
