# Map-safari

Code for a mapping game developed as part of my PhD. 

Game involves moving around the Acholi region of Northern Uganda looking for buildings to map. Features <a href="https://github.com/microsoft/Uganda-Tanzania-Building-Footprints" target="_blank">building footprints</a> produced by Microsoft. Users are required to verify outlines and edit them before they are uploaded to OpenStreetMap. 

Uses <a href="https://github.com/jonnyhuck/huckathon.org" target="_blank">tasking manager</a> designed by Jonny Huck. 

The website depends the following open source libraries: 

Leaflet.js (https://leafletjs.com/), Leaflet Path Transform (https://github.com/w8r/Leaflet.Path.Transform), Turf.js (https://turfjs.org/), OSM-Request (https://github.com/osmlab/osm-request), OSM-auth (https://github.com/osmlab/osm-auth), Leaflet-plugins (https://github.com/shramov/leaflet-plugins/blob/master/layer/tile/Bing.js) ; the following data sources: Bing Imagery (https://docs.microsoft.com/en-gb/bingmaps/rest-services/imagery/), Microsoft Uganda-Tanzania Building Geometries (https://github.com/microsoft/Uganda-Tanzania-Building-Footprints); 

And the following services: OSM APIv3 (https://wiki.openstreetmap.org/wiki/API_v0.6), Overpass API (https://wiki.openstreetmap.org/wiki/Overpass_API).




Beta - not for use until links between website pages are fixed and detailed explanation of database set up created. 


# To do:
<ul>
        <li>Fix broken links</li> 
        <li>Explanation of database set up</li>
</ul>   


