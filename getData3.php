<?php

header('Content-Type: application/json');

ini_set('max_execution_time', '150'); //300 seconds = 5 minutes

//update the square
require('./scripts/connection.php');



//Get contents of the post request and conver to array
$post = file_get_contents('php://input');
$array = json_decode($post, true);


//Get feature ID and XML file and declare in variables
$lat = $array["lat"];
$lon = $array["lon"];
$xml = $array["xml"];

//Parse the XML.
$xml = simplexml_load_string($xml);


//Get bounding box of buffered centre point

$bboxC = "SELECT ST_envelope(ST_Buffer(ST_SetSRID(ST_MakePoint($lat, $lon),4326), 0.015))";
$bbox = pg_fetch_row(pg_query($bboxC))[0];

//See if there is data within
$query = "SELECT ogc_fid, ST_AsText(ST_Transform(wkb_geometry,4326)) FROM acholi_buildings WHERE ST_Intersects(wkb_geometry, '$bbox')";
$result = pg_query($query);

$values = array();

$DBoutput    = '';
$rowOutput = '';
$OSMrowOutput = '';
$OSMoutput = '';

$OSMGeoms = array();

if (!$xml->way && pg_num_rows($result) > 0){ 

	while ($row = pg_fetch_row($result)) {
                	//Lock building in database 
                	//$lockQ = "UPDATE acholi_buildings SET STATUS = 1 WHERE ogc_fid = $row[0]";
                	//$lock = pg_query($lockQ);

                	//Convert database coordinates to geojson format
                	$conv = "SELECT ST_AsGeoJSON('SRID=4326; $row[1]')";
                	$DBGeom = pg_fetch_row(pg_query($conv))[0];

                	//Build geojson from database data
                	$rowOutput = (strlen($rowOutput) > 0 ? ',' : '') . '{"type": "Feature", "properties": {"source": "Database"}, "geometry": ' . $DBGeom . '}';
                	$DBoutput .= $rowOutput;

		//echo json_encode("No OSM");
        	}

        	$DBoutput = '{"type": "FeatureCollection","name": "buildings", "crs":{"type": "name", "properties": { "name": "urn:ogc:def:crs:OGC:1.3:CRS84"} }, "features": [ ' . $DBoutput . ' ]}';

		echo $DBoutput;

} else if ($xml->way && pg_num_rows($result) > 0){

	while ($row = pg_fetch_row($result)) {

		try {

		foreach($xml->way as $items) {



				if ($items->tag->attributes()->k == "building"){


					$coords = array();

					//Loop through the nodes of that way and get their ID
                                	foreach($items->nd as $node){
                                        	$nodeRef = $node->attributes()->ref;


                                        	$ref = (int)$nodeRef;


                                        	//Loop through the xml file and get the node IDs
                                        	foreach($xml->node as $node){


                                                	try {

                                                        	$id2 = $node->attributes()->id;

                                                        	//if node IDs match get coordinates and add to the array created above
                                                        	if ($id2 == $ref){
                                                                	//Get coordinates of the node and add to coordinates array
                                                                	$coord2 = $node->attributes()->lon . " " . $node->attributes()->lat;
                                                                	array_push($coords,$coord2);

                                                                };

                                                	} catch (Throwable $e){

						}


                                        };

                                };

                                //Remove commas between coordinates - creates a geometry string compatible with postgres = feature in OSM xml file
                                $array  = '(' . implode(",", $coords) . ')';

				
				//Array here is giving me different arrays for each poly and I think from here I want to check the intersection of them and if they intersect don't do anything with the poly
				//and if they don't need to get both get both sets of coordinates to plot

				//This isn't working
				//Step 3: Query intersection between the proposed geometry and the geometry in OSM
                    		$query = "SELECT ST_Overlaps('SRID=4326; POLYGON($array)', 'SRID=4326; $row[1]')";
                    		$intersects = pg_fetch_row(pg_query($query))[0];


				if($intersects == 't'){
                    
                    
                    array_push($values, "no buildings");


				} else {

					//Convert database coordinates to geojson format
                        		$conv = "SELECT ST_AsGeoJSON('SRID=4326; $row[1]')";
                        		$DBGeomI = pg_fetch_row(pg_query($conv))[0];

					//Convert OSM coordinates to geojson format
                                        $convOSMf = "SELECT ST_AsGeoJSON('SRID=4326; POLYGON($array)')";
                                        $OsmGeomf = pg_fetch_row(pg_query($convOSMf))[0];

					//Build geojson from database data
                                        $rowOutput = (strlen($rowOutput) > 0 ? ',' : '') . '{"type": "Feature", "properties": {"source": "Database"}, "geometry": ' . $DBGeomI . '}';
                                        $DBoutput .= $rowOutput;

					//Create geoJSON with OSM data
					//$rowOutput = (strlen($rowOutput) > 0 ? ',' : '') . '{"type": "Feature", "properties": {"source": "OSM"}, "geometry": ' . $OsmGeomf . '}';
                        		//$DBoutput .= $rowOutput;


				}

				} else {

					//echo json_encode("no building");

					//Convert database coordinates to geojson format
                                        $convNB = "SELECT ST_AsGeoJSON('SRID=4326; $row[1]')";
                                        $DBGeomNB = pg_fetch_row(pg_query($convNB))[0];


					//Build geojson from database data
                        		$rowOutput = (strlen($rowOutput) > 0 ? ',' : '') . '{"type": "Feature", "properties": {"source": "Database"}, "geometry": ' . $DBGeomNB . '}';
                        		$DBoutput .= $rowOutput;

				}


		}


		} catch(Throwable $e){

		}
	

	}


//Create final DB geoJSON with all outputs 
$DBoutput = '{"type": "FeatureCollection","name": "buildings", "crs":{"type": "name", "properties": { "name": "urn:ogc:def:crs:OGC:1.3:CRS84" } }, "features": [ ' . $DBoutput . ' ]}';
                
echo $DBoutput;

} else {

    echo json_encode("no buildings");

	
}
?>
