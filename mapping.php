<!DOCTYPE html>
<html>
        <head>
        
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">       
		<!-- if we are not in a session, go back to the start -->

		<!--<script src="../mapping-tool/scripts/initSurvey.js"></script>-->
		<script src="../mapping-tool/scripts/initSurveySeminar.js"></script>

		<link rel="shortcut icon" href="#">
	    <!--load osm-auth -->
        <script src="./scripts/osm-auth-main/osmauth.js"></script>

		<!-- load js require -->
		<script src="./node_modules/npm-require/src/npm-require.js"></script>
		<!-- Load CSS for Leaflet -->
		<link rel="stylesheet" href="https://unpkg.com/leaflet@1.7.1/dist/leaflet.css" />
		<!-- Load JavaScript for Leaflet -->
		<script src="https://unpkg.com/leaflet@1.7.1/dist/leaflet.js"></script>
		<!-- Load Leaflet Overview -->
		<script src="./scripts/leaflet-overview-master/src/leaflet-overview.min.js"></script>	
		<script rel="stylesheet" href="./scripts/leaflet-overview-master/src/leaflet-overview.css"></script>
		<!-- Load Javascript for Leaflet Path Transform -->
		<script src="./scripts/Leaflet.Path.Transform/dist/L.Path.Transform_v5.js"></script>
		<!-- Load Javascript for Leaflet Editable -->
		<script src="./scripts/Leaflet.Editable.js"></script>
		<!-- Load Javascript for Leaflet Context Menu -->
		<script src="./scripts/Map.ContextMenu.js"></script>
		<script src="./scripts/Mixin.ContextMenu.js"></script>
		<!-- Load CSS for Context Menu-->
		<link rel="stylesheet" href="./scripts/leaflet.contextmenu.css"/>
		<!-- Load Javascript for Turf -->
		<script src="https://cdn.jsdelivr.net/npm/@turf/turf@6.3.0/turf.min.js"></script>
		<!-- Leaflet rotated marker -->
		<script src="https://cdn.jsdelivr.net/npm/leaflet-rotatedmarker@0.2.0/leaflet.rotatedMarker.min.js"></script>
        <!-- Leaflet draw -->
		<script type="module" src="./node_modules/spin.js/spin.js"></script>
		<script src="https://cdn.jsdelivr.net/npm/leaflet-draw@1.0.4/dist/leaflet.draw.min.js"></script>
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/leaflet.draw/1.0.4/leaflet.draw.css" />
		<!-- Load Javascript for Bing tiles-->
		<script src="./scripts/Bing.js"></script>
		<script src="./scripts/Bing.addon.applyMaxNativeZoom.js"></script>  
		<!-- Load fonts -->
		<link href="https://fonts.googleapis.com/css2?family=Heebo:wght@400;600&display=swap" rel="stylesheet"> 
		<!-- Polyfill-->
		<script src="https://cdn.polyfill.io/v2/polyfill.min.js?features=Promise"></script>
		<link rel="stylesheet" href='./game.page2.css'/>
		<!--OSM to GeoJSON-->
		<script src="./scripts/osmtogeojson/osmtogeojson.js"></script>
		<script src="./walkthrough.js"></script>
		<script src="https://code.iconify.design/1/1.0.7/iconify.min.js"></script>       
		<script src="./scripts/leaflet-polygon.fillPattern.js"></script>
		<script src="./scripts/mapping.js"></script>
		<link rel="stylesheet" href="./node_modules/spin.js/spin.css">

	 </head>
        
        <body>

        <!-- the map -->


		<div id='container'>

        <div id="map">

		<div id="walkthroughStartPopUp" class="finishedPopUp" style="display:none;">
                <div class='modal' style="text-align:center; font-family: Arial; font-size: 16px">
				<p>Apwoyo bino!</p>
				<p>Are you ready to explore and take on the machine or do you need some adventurer training first?</p>
                <span><button style="background-color:#4CAF50; width: auto; height: 5%; margin:1%; padding:1%" id="training">I need some training!</button><button style="background-color:#fff080; width: auto; height: 5%; margin:1%; padding: 1%" id="noTraining">No, let's start now!</button></span>
                </div>
        </div>

		<div id="avatarSelection" class="finishedPopUp" style="width:35%; left:32.5%">
			<div class='modal' id="avatarHTML" style="text-align:center;font-family: Arial; font-size:16px">
			<p id="message">Apowyo bino!</p>
			<p id="message2">To begin please chose the character you want to explore Uganda with:</p>
			<div><button class="button buttonr" id="rhino"><img src='./Images/rhino.png' class="rhino"></button>
			<button class="button" id="giraffe"><img src='./giraffe.png' class="giraffe"></img></button>
			<button class="button buttone" id="elephant"><img src='./elephant.png' class="elephant"></button>
			<button class="button buttonh" id="hippo"><img src='./hippo.png' class="hippo"></button>
			<button class="button buttonl" id="lion" ><img src='./Images/lion.png' class="lion"></button>
			</div>
			</div>
		</div>

		<div id="editingDemo" class="finishedPopUp" style="display:none">

			<div id="demoContent" class="modal" style="text-align:center">
                                <span id="demoVideo"></span>
            </div>


		</div>

		<div id="walkthrough" class="walkthroughPopUp" style="display:none; top:78%;left:30%;width:40%;">

            <div id="walkthroughContent" class="modal" style="text-align:center; font-family: Arial; font-size: 16px">
				<p><span id="walkthroughHTML"></span></p>
            </div>
        </div>

        <div id="energyPopUp" class="countPopUp" style="display:none">
                <div class="modal">
                    <p><span id="popUp-message"></span> <b><span id="machine-points"></span> points</b>. Put it to good use and keep on mapping!</p>
                    <span><button style="background-color:#4CAF50; width: auto;" id="okay">Let's go!</button></span>
                </div>
        </div>

		<div id="finishedPopUp" class="finishedPopUp" style="display:none">

			<div class="modal" id="finished-message">
				<p>Great work! You found and uploaded <span id="buildingsUploaded"></span> buildings to OpenStreetMap! Have you finished mapping everything or are you giving up for now?</p>
                 <span><button style="background-color:#4CAF50; width: auto; height:auto" id="complete">I've finished mapping!</button><button style="background-color:#fff080; width: auto; height: auto; margin-left: 2%" id="givenUp">I've given up mapping!</button></span>
             </div>
        </div>

		<div id='banner'>

			<ul>

			<li id="instructions" style="display:inline-block; margin-top: 0.775%; margin-right: 1%"><a id="a" href="./scripts/instructions.php" target="_blank">Help!</a></li> 
			<li id="us_points" style="display:inline-block; margin-top: 0.75%; margin-right: 1%"><h1 style="width:auto"> Player points: <span id="user_points"></span></h1></li>
			<li id="m_points" style="display:inline-block; margin-top: 0.75%; margin-right: 1%"><h1 style="width:auto"> Computer points: <span id="machine_points"></span></h1></li>
			<li id="energy2" style="display:inline-block; margin-top: 1%; margin-right: 2%"><div id="energyBar" style="width:75%;max-width:100%;background-color: #04AA6D;"></div></li>
			<li id="finished" style="display:block; margin-right: 20px; width: 7%; margin-top:0.25%"><button onclick="finished()" style="width: 100%; height: auto; padding: 4%; background-color:#fff700; border: 2px solid #000000">I'm finished!</button></li>

			</ul>

		</div>

		<div id='banner2' style="width:100%;">
			<ul style="display:inline-block text-align:center;">
                <li style="float:none;display:inline-block"><button style="position:absolute; right:55%; width: auto; display: none; padding: 0.5%" id="showOutline">Show outline</button></li>
                <li style="float:none;display:inline-block"><button style="position:absolute; right:55%; width: auto; display: none; padding: 0.5%" id="hideOutline">Hide outline</button></li>
                <li style="float:none;display:inline-block"><button style="position:absolute; right:45%; width: auto; display: none; padding: 0.5%" id="circularise">Circularise</button></li>
                <li style="float:none;display:inline-block"><button style="position:absolute; right: 35%; width: auto; display:none; padding: 0.5%" id="reset">Reset feature</button></li>
            </ul>
		</div>
			
		<div id='sideButtons'>
			<div id='arrows'>
				<ul>
                    <li><button style="position:absolute;top:5%;right:50%; padding:0" id='up' onclick="move(this.id)"><span class="iconify" data-icon="akar-icons:arrow-up-thick" data-inline="false" id='arrow-span'></span></button></li>
					<li><button style="position:absolute;top:35%;right:37.5%;padding:0" id='right' onclick="move(this.id)"><span class="iconify" data-icon="akar-icons:arrow-right-thick" data-inline="false" id='arrow-span'></span></button></li>
					<li><button style="position:absolute;top:35%;right:62.5%;padding:0" id='left' onclick="move(this.id)"><span class="iconify" data-icon="akar-icons:arrow-left-thick" data-inline="false" id='arrow-span'></span></li>
					<li><button style="position:absolute;top:65%;right:50%;padding:0" id='down' onclick="move(this.id)"><span class="iconify" data-icon="akar-icons:arrow-down-thick" data-inline="false" id='arrow-span'></span></li>
                        
				</ul>

			</div>

				<ul>
				<li><button style="position:absolute;top:35%;right:1.5%;width: 9%; background-color: #ffffff" onclick="addBuilding()" id="add">Add own building</button></li>
				<li><button style="position:absolute;top:35%;right:1.5%;width: 9%; background-color: #4CAF50; display:none" id="uploadCreated">Upload building</button></li>
				</ul>

				<ul>
                <li><button style="position:absolute;top:35%;right:1.5%;width: 9%; background-color: #ffffff; display:none" id="addDemo">Add own building</button></li>
                <li><button style="position:absolute;top:35%;right:1.5%;width: 9%; background-color: #4CAF50; display:none" id="uploadCreatedDemo">Upload building</button></li>
                </ul>
				
				<ul>
				<li><button style="position:absolute;top:42.5%;right:1.5%;width: 9%; background-color: #4CAF50; display:none" id="upload">Upload building</button></li>
				<li><button style="position:absolute;top:48.5%;right:1.5%;width:9%; background-color:#008CBA; display:none " id="notSure">Not sure</button></li>
				<li><button style="position:absolute;top:54.5%;right:1.5%;width:9%; background-color: #f44336; display:none" id="notBuilding">Not a building</button></li>
				</ul>	

				<ul>
                <li><button style="position:absolute;top:42.5%;right:1.5%;width: 9%; background-color: #4CAF50; display:none" id="uploadDemo">Upload building</button></li>
                <li><button style="position:absolute;top:48.5%;right:1.5%;width:9%; background-color:#008CBA; display: none " id="notSureDemo">Not sure</button></li>
                <li><button style="position:absolute;top:54.5%;right:1.5%;width:9%; background-color: #f44336; display:none" id="notBuildingDemo">Not a building</button></li>
                </ul>

		</div>
		</div>

		<div id="overlay"></div>  

		</div>

 	<?php
        /* Gets a geometry from the database and creates a bounding box and echos it to the script */

		//Get chosen avatar
        $avatar = $_POST['avatarChoice'];

		//connect to database
        require('./scripts/connection.php');
                

		//Get the grid
		$query = "SELECT id, ST_XMin(b), ST_YMin(b), ST_XMax(b), ST_YMax(b), concat('https://www.openstreetmap.org/edit#map=18/', ST_y(a.c), '/', ST_x(a.c)),  distance from (select id, Box2D(ST_Transform(ST_Envelope(wkb_geometry), 4326)) as b, ST_AsText(ST_Transform(ST_Centroid(wkb_geometry), 4326)) as c, distance from grid WHERE status = 0 order by distance limit 1) a";
		$result = pg_query($query);

		//Initialise geometry variable
		$geom = '';

		//Get grid geometry and locks it 
		while ($row = pg_fetch_row($result)){

			$lock = pg_query("UPDATE grid SET status = 1 WHERE id = $row[0]");
			$query2 = "SELECT wkb_geometry FROM grid WHERE id = $row[0] ";
			$geom = pg_fetch_row(pg_query($query2))[0];

			//Get centroid of grid
			$centroid = pg_fetch_row(pg_query("SELECT ST_AsGeoJSON(ST_Transform(ST_Centroid(wkb_geometry), 4326)) FROM grid WHERE id = $row[0]"))[0];

			//Create string to get OSM data 
			$str2 = 'http://overpass-api.de/api/interpreter?data=[out:json][timeout:25];(way[%22building%22](' . $row[2] . ',' . $row[1] . ',' . $row[4] . ',' . $row[3] . ');relation[%22building%22](' . $row[2] . ',' . $row[1] . ',' . $row[4] . ',' . $row[3] . '););out%20body;%3E;out%20skel%20qt;';


			// create curl resource
 	        ch = curl_init();

			// set url
            curl_setopt($ch, CURLOPT_URL, $str2);

			//return the transfer as a string
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
 

			// $output contains the output string
			$OSM = curl_exec($ch);

			// close curl resource to free up system resources
			curl_close($ch);


			echo "<script>\n";
			echo "  var id = " . $row[0] . ";\n";
			echo "  var minx = " . $row[1] . ";\n";
			echo "  var miny = " . $row[2] . ";\n";
			echo "  var maxx = " . $row[3] . ";\n";
			echo "  var maxy = " . $row[4] . ";\n";
			echo " var centroid = $centroid \n";

            try {
					echo "var OSMXML = $OSM \n";   
				}
				catch (exception $e) {
				    //code to handle the exception
					echo "var OSMXL = '' \n";
				}

            echo "</script>\n";


		}

		//Get smaller grid 3 x 3 grid
		$smallGrid = pg_query("SELECT id, ST_AsGeoJSON(ST_Transform(wkb_geometry,4326)) FROM grid_small WHERE ST_Contains('$geom',wkb_geometry)");

		//Initiliase empty variables to store geometry
		$smallerGrid = '';
		$gridRow = '';


		while($row = pg_fetch_row($smallGrid)){

			$gridRow = (strlen($gridRow) > 0 ? ',' : '') . '{"type": "Feature", "id": ' . $row[0] . ', "properties": { "status": 0}, "geometry": ' . $row[1] . '}';

			$smallerGrid .= $gridRow;

			}

			//Get smaller grid as a geojson 
			$smallerGrid = '{"type": "FeatureCollection","name": "grid", "crs":{"type": "name", "properties": { "name": "urn:ogc:def:crs:OGC:1.3:CRS84" } }, "features": [ ' . $smallerGrid . ' ]}';

		//Select buildings from database that intersect the grid
		$queryB = "SELECT gid, ST_AsGeoJSON(ST_Transform(geom,4326)) FROM buildings WHERE ST_Intersects(ST_Transform(geom, 4326), ST_Transform('$geom', 4326))";
		$resultB = pg_query($queryB);

		//If there are buildings
		//Create geojson of the buildings
		if (pg_num_rows($resultB) > 0){

			$output    = '';
			$rowOutput = '';

            while ($row = pg_fetch_row($resultB)) {
                                $rowOutput = (strlen($rowOutput) > 0 ? ',' : '') . '{"type": "Feature", "id": ' . $row[0] . ', 									"properties": {}, "geometry": ' . $row[1] . '}';
                                $output .= $rowOutput;
                        }

                $output = '{"type": "FeatureCollection","name": "buildings", "crs":{"type": "name", "properties": { "name": "urn:ogc:def:crs:OGC:1.3:CRS84" } }, "features": [ ' . $output . ' ]}';


        } else {

                         $output = json_encode('no buildings');

        }

	
		echo "<script>\n";
		echo "let buildings = $output\n";
		echo "let smallGrid = $smallerGrid\n";
		echo "let test = $outputG\n";
		echo "</script>\n";

			
        ?>

        </body>
<script>



		//Declare global variables
		let avatar, avatarIcon, avatarIconFlip, avatarChoice;
		let buildingsLayer, building, osmLayer, osmGeo, map, smallerGrid, overlay, refGrid, drawnItems, drawControl, drawnBuildings, circle, food, energy, userName, buildingCount;
		let pointSound, leafSound, tiredSound, name;
		let activeBuilding, moveDistance, zoom, demoFeature, coords, videoCount, backUp;
		let editing; 
		let changeset;		

		//Game sounds
		pointSound = new sound("./sounds/points.mp3");
		leafSound = new sound("./sounds/eating.mp3");

		//Set energy variable as the energy bar item 
		energy = document.getElementById("energyBar");

		//Initialise building count for the square
		buildingCount = 0;

		//document.getElementById('energy-val').innerHTML = energy;

		try {
		//Convert OSM file to JSON	
		osmGeo = osmtogeojson(OSMXML);

		} catch(e){

			location.reload();

		}


		//Import osm-request library
		var OsmRequest = require('osm-request');

		//Main OSM api
		const osm = new OsmRequest({
                endpoint: 'https://www.openstreetmap.org',
                oauthConsumerKey: 'Na9yRlHHno5HaMhQUg4B58nscYrBdA7uiyvV52mq',
                oauthSecret: '27cJAoPleuQQxaD6svDcpis5LC7Tg4Lc0HvQ2Yl5',
                auto: true});	


		//Add an event listener to the green button which when clicked uploads the selected building
        document.getElementById("giraffe").addEventListener("click", function(event){

					console.log(document.getElementById("message").innerHTML);
					document.getElementById("message").innerHTML = "LOADING...";
					document.getElementById("message2").innerHTML = "";

				})


		//If not authenticated bring up log in pop up window
		if(!osm._auth.bringPopupWindowToFront()) {
                	osm._auth.authenticate(function() {
			

        	        });
	         };


	/**
	* Function that gets authenticated user details from OSM
	*  Returns as a promise
	*/

	function  authUser(){

		let req = new Promise(function(resolve, reject) {

			let init = osm._auth.xhr({
                        	method: 'GET',
                        	path: '/api/0.6/user/details'
                	}, done);

			//DOM parser so xml can be read
			parser = new DOMParser();		
			init.onreadystatechange = function () {

            // check the response is OK
		    //If okay resolve response text - osm xml file
            if (this.readyState === 4) {

						resolve(parser.parseFromString(this.responseText,"text/xml"));
                    }}
		});

		
		return req;

		}
	
		/**
		* Async function which gets the OSM user details of the tool user
		* TO DO: Combine the above?
		*/
		async function initUser(){

			let user = await authUser();

			return user;

		} 

		//If user authenticated
		if(osm._auth){
		
			//initUser function
			initUser()
				.then((result) => {
					//With result get the user details
					let usr = result.getElementsByTagName('user')[0];

					//Get username from user details, put into a variable name and update username HTML element
					let usrName = {
              					  display_name: usr.getAttribute('display_name'),
            				};
					userName = usrName["display_name"];


					//Get user count
					let promise = makeRequestPromise(['./scripts/findUserPoints.php?user=',userName].join(''));

                                return promise;
                        })
			.then((result)=> {
                
				console.log(result);

				//Update buildings mapped HTML element
				document.getElementById("user_points").innerHTML = result[0];

				//Update buildings mapped HTML element
                document.getElementById("machine_points").innerHTML = result[1];

				//check if user has completed walkthrough
				let promise = makeRequestPromise(['./scripts/walkthroughStatus.php?user=', userName].join(''));

				return promise

			}).then((result)=> {

				//Add an event listener to the green button which when clicked uploads the selected building
				document.getElementById("giraffe").addEventListener("click", function(event){
				document.getElementById("message").innerHTML = '<p>LOADING...</p>';
				document.getElementById("message2").innerHTML = ''

				avatarSelect('giraffe', result);

				});

				//Add an event listener to the green button which when clicked uploads the selected building
                document.getElementById("hippo").addEventListener("click", function(event){

							document.getElementById("avatarHTML").innerHTML = 'LOADING...';

							avatarSelect('hippo', result);

                });

				//Add an event listener to the green button which when clicked uploads the selected building
                document.getElementById("elephant").addEventListener("click", function(event){


						document.getElementById("avatarHTML").innerHTML = 'LOADING...';

						avatarSelect('elephant', result);

                });
			
				//Add an event listener to the green button which when clicked uploads the selected building
				document.getElementById("rhino").addEventListener("click", function(event){

						document.getElementById("avatarHTML").innerHTML = 'LOADING...';

						avatarSelect('rhino', result);

				});

				//Add an event listener to the green button which when clicked uploads the selected building
				document.getElementById("lion").addEventListener("click", function(event){


						document.getElementById("avatarHTML").innerHTML = 'LOADING...';

						avatarSelect('lion', result);

				});


			})
			

function avatarSelect(avatarChoice, walkthrough){

	//Set sound, avatar picture and name based on user selection 
	if(avatarChoice == 'giraffe'){


                name  = "Mirembe";

				let icon = './giraffe.png';
						let shadow = './giraffe_shadow.png';

						tiredSound = new sound("./sounds/horse.mp3");

						avatarIcon = L.icon({
								iconUrl: icon,
								iconSize:     [38, 105], // size of the icon
								iconAnchor:   [0, 0], // point of the icon which will correspond to marker's location
								
						});


				avatarIconFlip = L.icon({
                                iconUrl: './Images/giraffe_l.png',
								iconSize:     [38,105],
								iconAnchor:[0,0]
				});

    } else if(avatarChoice == 'elephant'){

                tiredSound = new sound("./sounds/elephant.mp3");

				name = "Dembe";

                 let icon = './elephant.png';
                avatarIcon = L.icon({
						iconUrl: icon,
						iconSize:     [105, 60], // size of the icon
						iconAnchor:   [0, 0], // point of the icon which will correspond to marker's location
                                                
                });


				avatarIconFlip = L.icon({
									iconUrl: './Images/elephant_l.png',
									iconSize:     [105, 60],
									iconAnchor:[0,0]
							});

				} else if (avatarChoice == 'rhino'){
					tiredSound = new sound("./sounds/hippo.mp3");

						name = "Sanyu";

						let icon = './Images/rhino_right.png';
						avatarIcon = L.icon({
						iconUrl: icon,
						iconSize:     [90, 60], // size of the icon
						iconAnchor:   [0, 0], // point of the icon which will correspond to marker's location
                                              
                                });
					avatarIconFlip = L.icon({
							iconUrl: './Images/rhino.png',
							iconSize:     [90, 60], // size of the icon
							iconAnchor:   [0, 0], // point of the icon which will correspond to marker's location

				});


        }else if (avatarChoice == 'lion'){

					tiredSound = new sound("./sounds/hippo.mp3");

					name = "Akiki";

					let icon = './Images/lion.png';

					avatarIcon = L.icon({
					iconUrl: icon,
					iconSize:     [110, 80], // size of the icon
					iconAnchor:   [0, 0], // point of the icon which will correspond to marker's location
                                                
        			});
					avatarIconFlip = L.icon({
						iconUrl: './Images/lion_l.png',
						iconSize:     [110, 80], // size of the icon
						iconAnchor:   [0, 0], // point of the icon which will correspond to marker's location

                     });


		}else {
                    tiredSound = new sound("./sounds/hippo.mp3");

					name = "Namazzi";
					let icon = './hippo.png';
				
					avatarIcon = L.icon({
							iconUrl: icon,
							iconSize:     [95, 55], // size of the icon
							iconAnchor:   [0, 0], // point of the icon which will correspond to marker's location
                                                                                });


					avatarIconFlip = L.icon({
							iconUrl: './Images/hippo_l.png',
							iconSize:     [95, 55],
							iconAnchor:[0,0]
					});


                                }
		
		/*

		if(walkthrough == 'yes'){

			document.getElementById("walkthroughStartPopUp").style.display = 'none';

		} else {

			document.getElementById("walkthroughStartPopUp").style.display = 'block';

		}*/

		//Make Map
		makeMap();
}
}
</script>
</html>
