<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">		
		<link rel="shortcut icon" href="#">
                <!--load osm-auth -->
                <script src="./scripts/osm-auth-main/osmauth.js"></script>
                <!-- load js require -->
                <script src="./node_modules/npm-require/src/npm-require.js"></script>
                <!-- Load CSS for Leaflet -->
                <link rel="stylesheet" href="https://unpkg.com/leaflet@1.7.1/dist/leaflet.css" />
                <!-- Load JavaScript for Leaflet -->
                <script src="https://unpkg.com/leaflet@1.7.1/dist/leaflet.js"></script>
            
                <!-- Load CSS for Context Menu-->
                <link rel="stylesheet" href="./scripts/leaflet.contextmenu.css"/>
                <!-- Load Javascript for Turf -->
		 <script src="https://cdn.jsdelivr.net/npm/@turf/turf@6.3.0/turf.min.js"></script>
                <!-- Load Javascript for Bing tiles-->
                <script src="./scripts/Bing.js"></script>
                <script src="./scripts/Bing.addon.applyMaxNativeZoom.js"></script>
                <!-- Load fonts -->
                <link href="https://fonts.googleapis.com/css2?family=Heebo:wght@400;600&display=swap" rel="stylesheet">
                <!-- Polyfill-->
                <script src="https://cdn.polyfill.io/v2/polyfill.min.js?features=Promise"></script>
		<script  src="./data/acholi_outline.geojson"></script>        

		<style>
            body, html{
                        width: 100%;
                        height: 100%;
                        margin: 0;
                }

            /* The map */
            #map {

			position: relative;
			left: 12.5%;
                        width: 70%;
                        height: 75%;
                        z-index: 0;
			text-align: center;
                }
        
            
	ul {
              list-style-type: none;
              margin: 0;
              padding: 0;
              overflow: hidden;
              width: 100%;
                   
                    
                }
            .ul2{
                height:7%;
		background-image: linear-gradient(rgba(247, 249, 251, 0.4), rgba(247, 249, 251, 0.4)), url(./Images/logo_world.png);
                background-position: center;
                background-size: 300px 300px;
                position: relative;
                border-bottom: 1px solid gray;
                
            }

		.ul3{
                display:inline-block;
            }


	   .main-head{
		text-align: center; 
		position: absolute;
		top: 50%;
		left: 50%;
		transform: translate(-50%, -50%);
		color: #93B292;
		font-family: 'Heebo', sans-serif;
		font-size:50px;
		text-shadow: -1px -1px 0 #546A58, 1px -1px 0 #546A58, -1px 1px 0 #546A58, 1px 1px 0 #546A58;

		}

	p {
                text-align: justify;
                text-justify: inter-word;
                float: center;
                display: inline-block;
                font-family: 'Heebo', sans-serif;
                font-size: 17px;
                line-height: 2;
                max-width: 90%;
                position: relative;
                left: 50%;
                transform: translate(-50%);
                
            }

		li {

			 position: relative;
                  	top:25%;
			float: right;
                    text-align: justify;
                    text-justify: inter-word;
                    display:inline-block;
                }

		.li2 {
			position:relative;
			text-align: justify;
                    text-justify: inter-word;
                    display:inline-block;

		}

                li a {
                  display: inline-block;
                  color: black;
                  text-align: center;
                  padding: 8px 16px;
                  text-decoration: none;
                  font-family: 'Noto Sans TC', sans-serif;
                    font-weight: 700;
                }

                li h {
                    display: block;
                    color: black;
                    text-align: center;
                    padding: 8px 16px;
                    text-decoration: none;
                    font-family: 'Noto Sans TC', sans-serif;
                    font-weight: 700;
                    font-size: 16pt;

                }



		/* style the container */
		#button-container {
			text-align: center;
			width: 100%;
		}
	
		/* style the container */
                #login-container {
                        text-align: center;
                        width: 100%;
                }
		
		/* Button styles */
		button {
			display: block;
                    color: black;
                    text-align: center;
                    padding: 8px 16px;
                    text-decoration: none;
                    font-family: 'Noto Sans TC', sans-serif;
                    font-weight: 700;
		    background-color: transparent;
			border: none;
                    font-size: 12pt;
		    cursor:pointer;

		}




		/*Legend specific*/
		.legend {
		  padding: 6px 8px;
		  font: 14px Arial, Helvetica, sans-serif;
		  background: white;
		  background: rgba(255, 255, 255, 0.8);
		  line-height: 24px;
		  color: black;
		}

		.legend span {
		  position: relative;
		  bottom: 3px;
		}

		.legend i {
		  width: 18px;
		  height: 18px;
		  float: left;
		  margin: 0 8px 0 0;
		  opacity: 0.7;
		  border: solid 0.5px black;
		}

		.legend i.icon {

		  background-size: 18px;
		  background-color: rgba(255, 255, 255, 1);
		}

		.footer{
                background-color: #7fcdbb;
                width: 100%;
		height:100px;
                color: white;
                font-family: 'Noto Sans TC', sans-serif;
                text-align: center; 
                
            }
            
	    .footer .footer-section{
		   text-align: center; 
            }

		img {
			height: 25px;
			width: 25px;

		}

	
		.my-label {
		position: absolute;
    		font-size:12px;
		background-color:transparent;
		border:none;
		box-shadow: none;
		pointer-events: none;
		font-weight: bold;
		text-shadow: 1.5px 1.5px white, -1.5px -1.5px white, -1.5px 1.5px white, 1.5px -1.5px white;
		}

		.leaflet-tooltip-top:before, 
		.leaflet-tooltip-bottom:before, 
		.leaflet-tooltip-left:before, 
		.leaflet-tooltip-right:before {
	    		border: none !important;
		}

		.circle {
   			 display: table-cell;
    			text-align: center;
    			vertical-align: middle;
    			border-radius: 50%;
    			border-style: solid;
    			font-size: 16px;
    			font-weight: bold;
			background-color:white 
 		}

		 @viewport {
                        width: device-width ;
                        zoom: 1.0 ;
                }


		@media screen and (max-width: 1500px) and (min-width: 1100px) {

		p {
			font-size: 15px;

		}

		.main-head{
			font-size: 45px;

		}

		li a {
                  padding: 8px 20px;
	

                }

		.ul3 {
			height:5%;
		}

		}

		</style>
	</head>
	
	<body>
		 <ul class="ul ul2"> <h class="main-head">ABOUT</h>
            	<li><a href="../home.html">HOME</a></li>
            	<!--<li><a href="./home.php">HOME</a></li>-->
            	<!--<li><button class="button" href="./stats2.php" onclick="statsPage()">PROFILE</a></li>-->
		</ul>

		
		    <p>The two tools are part of a larger PhD research project carried out by Kirsty Watkinson at the University of Manchester seeking to create maps of un-mapped parts of the Global South to help address humanitarian and development challenges. It combines a volunteer (you!) with machine learning to speed up the mapping process. By doing this we can map as accurately as a human, but cover much larger areas than if we were to do it alone. This helps us to map remote areas of the world desperately in need of map data to assist humanitarian response, support national development and contribute to the monitoring of global initiatives such as the <b><a href='https://sdgs.un.org/goals' target='_blank'>Sustainable Development Goals</a></b>. Map Safari also contributes to the work of the <b><a href='https://spark.adobe.com/page/SiM4Iji37rdJr/' target='_blank'>Community Mapping Company</a></b>, also based at the University of Manchester. <br>
            	<br>All data produced will be uploaded to <b><a href='https://www.openstreetmap.org/' target='_blank'>OpenStreetMap</a> </b> where it will be available for free to all.
            	<br>
		<br> Currently we have mapped <b><span id='area'></span> km<sup>2</sup></b> of the Acholi region. See our progress below: 

		<div id="map"></div>
<br>
		<div class="footer">
            <div class="footer-section">
                <!--<ul class="ul ul3">
                <li class="li2"><a href="https://www.instagram.com/community_mapping_ug/" target="_blank"><img src="./images/instaw.png" alt="Instagram"></a></li>
                <li class="li2"><a href="https://twitter.com/klwatkinson" target="_blank"><img src="./images/twitter.png" alt="Twitter"></a></li>
		<li class="li2"><a href="https://www.youtube.com/channel/UCngCyaEIbO9ACjXrroBd5ow/featured" target="_blank"><img src="./images/youtube.png" alt="YouTube"></a></li></ul> 
		</div>-->
		<div><p style="float:left">Contact: kirsty.watkinson@manchester.ac.uk<span style="color:transparent">test</span> <a href="https://www.instagram.com/community_mapping_ug/" target="_blank"><img src="./images/instaw.png" alt="Instagram"></a><span style="color:transparent">test</span> <a href="https://twitter.com/klwatkinson" target="_blank"><img src="./images/twitter.png" alt="Twitter"></a><span style="color:transparent">test</span> Credits: <a target="_blank" href="https://iconify.design/">Iconify</a>, <a target="_blank" href="https://wordart.com/">Word Art</a></p>
            </div>
		</div>

		<?php

			require('./scripts/connection.php');

			$outline = pg_fetch_row(pg_query("SELECT ST_AsGeoJSON(ST_Transform(wkb_geometry, 4326)) FROM outline"))[0];

			$outlineGeom = pg_fetch_row(pg_query("SELECT wkb_geometry FROM outline"))[0];

			$grid = '';
                	$gridRow = '';			

			$gridQuery = pg_query("SELECT id, status, ST_AsGeoJSON(ST_Transform(wkb_geometry, 4326)) FROM grid");

			 $gridQuery2 = pg_query("SELECT id, status, ST_AsGeoJSON(ST_Transform(wkb_geometry, 4326)) FROM grid WHERE ST_Intersects(ST_Transform(wkb_geometry,4326), ST_Transform('$outlineGeom',4326))");

			 $test = pg_fetch_row($gridQuery2)[0];

			while ($row = pg_fetch_row($gridQuery2)){

				$gridRow = (strlen($gridRow) > 0 ? ',' : '') . '{"type": "Feature", "id": ' . $row[0] . ', "properties": { "status": ' . $row[1] . '}, "geometry": ' . $row[2] . '}';

                        	$grid .= $gridRow;

                        }

                        $grid = '{"type": "FeatureCollection","name": "grid", "crs":{"type": "name", "properties": { "name": "urn:ogc:def:crs:OGC:1.3:CRS84" } }, "features": [ ' . $grid . ' ]}';




			//Get squares

			$score = "test";

			echo "<script>\n";
			echo "let grid = $grid\n";

			echo "let out = $outline\n";
			echo"</script>";



			

		?>
<script>
	

		var OsmRequest = require('osm-request');

        const osm = new OsmRequest({
                endpoint: 'https://www.openstreetmap.org',
                oauthConsumerKey: 'Na9yRlHHno5HaMhQUg4B58nscYrBdA7uiyvV52mq',
                oauthSecret: '27cJAoPleuQQxaD6svDcpis5LC7Tg4Lc0HvQ2Yl5',
                auto: true});
/*
		const osm = new OsmRequest({
                endpoint: 'https://api06.dev.openstreetmap.org',
                oauthConsumerKey: 'VnCASv0JrF4mqHQ3uWBCE6bf1dHFh14ltHoGBbmV',
                oauthSecret: '9fAkZCyRDkS8yAySv9MJYf78uFM8wcERG3oK1081',
                auto: true});
*/
		if (!osm._auth.bringPopupWindowToFront()) {
                                osm._auth.authenticate(function() {
                                        //Update function
                                        showDetails();
                                });
                        } else {
				showDetails();

		}


                
        //Bounds of Gulu district
        min_x = 32.12395;
        max_x = 32.82805;
        min_y = 2.4515;
        max_y = 3.3066;


        //set up map
        //Create a variable containingthe map object and add the basemaps as layers
        map = L.map('map', {minZoom: 9,
                            maxZoom: 10,
                            zoomControl: false
                        });
            
        //Load bing and OpenStreetMap basemaps
        const bing = L.bingLayer('AgWUG-xtDgsFNlDhzIMjiuvn2sqdQ82E_ywrY3jp_mbrOiIgW9w7YJ39oJiKwteg', {
                               maxZoom: 18}).addTo(map);


                                        
	let gridColour = L.geoJSON(grid);
	
	let gridDisplay = L.geoJSON();

	let area = 0;

	let acholiOut = turf.polygon(acholiBoundary.features[0].geometry.coordinates[0]);

	let acholiGrid = L.geoJSON(); 

	gridColour.eachLayer(function(layer){


		if (layer.feature.properties.status === 0){
			layer.setStyle({fillColor: '#edf8b1', weight: 0, fillOpacity: 0.6})
		
		} else if (layer.feature.properties.status === 1 || layer.feature.properties.status === 3){
			layer.setStyle({fillColor: '#7fcdbb', weight: 0, fillOpacity: 0.6})
			
		} else {

			layer.setStyle({fillColor: '#2c7fb8', weight: 0, fillOpacity: 0.6})

			area += 1;
		}


		});

	document.getElementById('area').innerHTML = area;

	gridColour.addTo(map);

	

	let gridOutline = L.geoJSON(grid, {style: {color: 'black', weight: 0.75, fillOpacity: 0}}).addTo(map);
	
	let acholiOutline = L.geoJSON(out ,{style: {color:'black', fillOpacity:0}}).addTo(map);

	 let gridBounds = acholiOutline.getBounds();

	var size = 2 * 2;
  	var style = 'style="width: ' + size + 'px; height: ' + size + 'px; border-width: ' + 1 + 'px;"';
  	var iconSize = size + (2 * 2);
  	var icon = L.divIcon({
  		  html: '<span class="circle"' + style + '> </span>',
 		   className: '',
 		   iconSize: [iconSize, iconSize]
 	 });


	let Gulu = new L.marker([2.4654,32.1757],{icon:icon}); 

	//opacity may be set to zero
	Gulu.bindTooltip("Gulu", {permanent: true, className: "my-label", offset: [-25, -15] });
	Gulu.addTo(map);

	let amuru = L.marker([2.4907,31.5151], {icon:icon }); //opacity may be set to zero
        amuru.bindTooltip("Amuru", {permanent: true, className: "my-label", offset: [-30, -15] });
        amuru.addTo(map);

	
	let pader = L.marker([2.5240,33.0506], { icon: icon }); //opacity may be set to zero
        pader.bindTooltip("Pader", {permanent: true, className: "my-label", offset: [-25, -15] });
        pader.addTo(map);

	let kitgum = L.marker([3.1720,32.524], {icon: icon }); //opacity may be set to zero
        kitgum.bindTooltip("Kitgum", {permanent: true, className: "my-label", offset: [-30, -15] });
        kitgum.addTo(map);

	

	let legend = L.control({position: 'bottomright'});

	legend.onAdd = function(map) {
  		let div = L.DomUtil.create("div", "legend");
  			div.innerHTML += '<i style="background: #2c7fb8"></i><span>Mapped</span><br>';
  			div.innerHTML += '<i style="background: #7fcdbb"></i><span>In progress</span><br>';
  			div.innerHTML += '<i style="background: #edf8b1"></i><span>Not mapped</span><br>';
  
  		return div;
	};

	legend.addTo(map);

	map.fitBounds(gridBounds);

	if(document.body.clientWidth <1367){
		map.setZoom(9);
	}

	function statsPage(){
                        let f = document.createElement('form');
                                f.action = './stats2.php';
                                f.method = 'POST';

                                let i = document.createElement('input');
                                i.type ='hidden';
                                i.name ='user';
                                i.value = userName;
                                f.appendChild(i);


                                document.body.appendChild(f);
                                f.submit();


                }

		//Shows user name in top right corner - indicates they are logged in
                function done(err, res) {
                        // Error display
                        if (err) {
				console.log(err);

                        }

                        //Get username from returned xml
                        var u = res.getElementsByTagName('user')[0];
                        var o = {
                                display_name: u.getAttribute('display_name')
                        };

                        //set inner html of the user element as the username
                        for (var k in o) {
                                console.log(o[k]);
                                userName = o[k];
                        }

                }

                //Get user details using the osm-request library
                function showDetails() {
                        osm._auth.xhr({
                        method: 'GET',
                        path: '/api/0.6/user/details'
                        }, done);

                }

                 
        
		</script>
	</body>

</html>
